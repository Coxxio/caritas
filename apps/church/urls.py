from django.urls import path
from .views import *

urlpatterns = [
    path('create-church',create_church.as_view(), name = 'create_church'),
    path('delete-church/<int:pk>',delete_church.as_view(), name = 'delete_church'),
    path('create-church-group',create_church_group.as_view(), name = 'create_church_group'),
    path('create-integrant',integrant_church_groupCreateView.as_view(), name = 'create_integrant'),
    path('create-nursery',nurseryCreateView.as_view(), name = 'create_nursery'),
    path('list-church-group',list_church_group.as_view(), name = 'list_church_group'),
    path('list-church',list_church.as_view(), name = 'list_church'),

]
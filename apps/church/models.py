from django.db import models
from apps.homes.models import sector,parish

class church(models.Model):
    """Model definition for church."""

    church = models.AutoField(primary_key=True)
    name_church = models.CharField('Nombre de la Iglesia', max_length=50,blank=False,null=False)
    direction = models.CharField('Direccion de la Iglesia', max_length=50,blank=False,null=False)
    parish = models.ForeignKey(parish, on_delete=models.CASCADE,default=809)
    sector = models.ManyToManyField(sector)

    class Meta:
        """Meta definition for iglesia."""

        verbose_name = 'iglesia'
        verbose_name_plural = 'iglesias'

    def __str__(self):
        """Unicode representation of iglesia."""
        return self.name_church



class church_group(models.Model):
    """Model definition for grupo_parroquial."""

    church_group = models.AutoField(primary_key=True)
    name_church_group = models.CharField('Nombre del Grupo Parroquial', max_length=100, blank=False, null=False)
    church = models.ForeignKey(church, on_delete=models.CASCADE)

    class Meta:
        """Meta definition for grupo_parroquial."""

        verbose_name = 'grupo_parroquial'
        verbose_name_plural = 'grupo_parroquiales'

    def __str__(self):
        """Unicode representation of grupo_parroquial."""
        return self.name_church_group





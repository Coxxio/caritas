from .models import *
from apps.homes.models import *
from django.forms import *

class ubicationchurchForm(Form):
    """ubicationForm definition."""

    state = ModelChoiceField(queryset=state.objects.all(), 
    widget=Select(attrs={
        'class':'form-select'
    }))

    municipality = ModelChoiceField(queryset=municipality.objects.none(), 
    widget=Select(attrs={
        'class':'form-select'
    }))

    parish = ModelChoiceField(queryset=parish.objects.none(), 
    widget=Select(attrs={
        'class':'form-select',
        'id':'parish_church'
    }))
    

    community = ModelChoiceField(queryset=community.objects.none(), 
    widget=Select(attrs={
        'class':'form-select'
    }))

    sector = ModelChoiceField(queryset=sector.objects.none(), 
    widget=Select(attrs={
        'class':'form-select'
    }))

class churchForm(ModelForm):
    """Form definition for church."""
    parish = ModelChoiceField(queryset=parish.objects.filter(id_municipality=309), 
    widget=Select(attrs={
        'class':'form-select',
        'id':'parish_church'
    }))

    class Meta:
        """Meta definition for churchform."""

        model = church
        fields = (
            'name_church',
            'direction',
            'parish',
            'sector'
        )
        labels = {
            'sector':'Sectores atendidos por la iglesia'
        }
        widgets = {
            'name_church': TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'"Nuestra Señora de Fatima"'
                }
            ),
            'direction': TextInput(
                attrs={
                    'class':'form-control',
                    'placehodler':'Direccion'
                }
            ),
            'sector': CheckboxSelectMultiple(
                attrs={
                    'class':'form-checkbox'
                }
            )
        }

class church_groupForm(ModelForm):
    """Form definition for church_group."""

    class Meta:
        """Meta definition for church_groupform."""

        model = church_group
        fields = (
            'name_church_group',
            'church'
        )
        labels = {
            'church':'Iglesia'
        }
        widgets = {
            'name_church_group': TextInput(
                attrs={
                    'class':'form-control'
                }
            ),
            'church': Select(
                attrs={
                    'class':'form-control',
                    'onchange':'list_gruop();'
                }
            )
        }


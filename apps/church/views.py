from django.shortcuts import render, redirect
from django.views.generic import TemplateView, DeleteView, CreateView
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http.response import JsonResponse

from apps.childs.models import integrant_church_group, nursery
from .forms import *
from apps.childs.forms import integrantForm, nurseryForm
from apps.homes.forms import *
from apps.homes.models import *

class create_church(TemplateView):
    template_name = "church/create_church.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save()
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        data = {}
        form = churchForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('church:list_church')
        else:
            print(form.errors)
        try:
            action = request.POST['action']
            if action == 'search_parish_church':
                id = request.POST['id']
                data = []
                for i in parish.objects.raw('SELECT * FROM homes_sector, homes_community, homes_parish where homes_parish.id_parish=%s AND homes_parish.id_parish=homes_community.id_parish_id AND homes_community.id_community=homes_sector.id_community_id',[id]):
                    print(i)
                    data.append({'id': i.id_sector, 'name': i.name_sector})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationchurchForm()
        context['form'] = churchForm()
        return context

class create_church_group(TemplateView):
    template_name = "church/create_church_group.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save()
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        data = {}
        form = church_groupForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
        try:
            action = request.POST['action']
            if action == 'search_municipality_id':
                data = []
                for i in municipality.objects.filter(id_state=request.POST['id']):
                    data.append({'id': i.id_municipality, 'name': i.name_municipality})
            elif action == 'search_parish_id':
                data = []
                for i in parish.objects.filter(id_municipality=request.POST['id']):
                    print(i)
                    data.append({'id': i.id_parish, 'name': i.name_parish})
            elif action == 'search_church':
                id = request.POST['id']
                data = []
                k = 0
                for i in parish.objects.raw('SELECT * FROM homes_sector, homes_community, homes_parish, church_church where homes_parish.id_parish=%s AND homes_parish.id_parish=homes_community.id_parish_id AND homes_community.id_community=homes_sector.id_community_id',[id]):
                    if k == 0:
                        data.append({'id': i.church, 'name': i.name_church, 'direction': i.direction})
                        j = i.church
                        k+=1
                    elif j != i.church:
                        j = i.church
                        data.append({'id': i.church, 'name': i.name_church, 'direction': i.direction})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form'] = church_groupForm()
        return context

class list_church_group(TemplateView):
    template_name = "church/list_church_group.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_municipality_id':
                data = []
                for i in municipality.objects.filter(id_state=request.POST['id']):
                    data.append({'id': i.id_municipality, 'name': i.name_municipality})
            elif action == 'search_parish_id':
                data = []
                for i in parish.objects.filter(id_municipality=request.POST['id']):
                    print(i)
                    data.append({'id': i.id_parish, 'name': i.name_parish})
            elif action == 'search_church':
                id = request.POST['id']
                data = []
                for i in church.objects.filter(parish=id):
                    data.append({'id': i.church, 'name': i.name_church, 'direction': i.direction})
            elif action == 'search_group':
                id = request.POST['id']
                data = []
                for i in church_group.objects.filter(church=id):
                    j = integrant_church_group.objects.filter(church_group=i.church_group).count()
                    k = nursery.objects.filter(church_group=i.church_group).count()
                    try:
                        l = integrant_church_group.objects.filter(church_group=i.church_group,rank='Presidente').count()
                        m = integrant_church_group.objects.filter(church_group=i.church_group,rank='Presidente')
                        if l:
                            data.append({'id': i.church_group, 'name': i.name_church_group,'integrants':j,'nursery':k,'president_name':m[0].person.name_person, 'president_last_name':m[0].person.last_name_person})
                        else:
                            data.append({'id': i.church_group, 'name': i.name_church_group,'integrants':j,'nursery':k,'president_name':'No Existe Presidente', 'president_last_name':'Asignado'})
                    except Exception as e:
                        data['error'] = str(e)
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form'] = church_groupForm()
        return context


class integrant_church_groupCreateView(CreateView):
    model = integrant_church_group
    template_name = "church/create_integrant.html"
    form_class = integrantForm

    def post(self, request,*args, **kwargs):
        if request.is_ajax:
            form = self.form_class(request.POST)
            if form.is_valid():
                form.save()
                mensaje = 'registrado correctamente!'
                error = 'No hay error!'
                response = JsonResponse({'mensaje':mensaje,'error':error})
                print(mensaje)
                response.status_code = 201
                return response
            else:
                mensaje = f'{self.model.__name__} no se ha podido registrar!'
                error = form.errors
                print(error)
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('church:list_church_group')


class nurseryCreateView(CreateView):
    model = nursery
    template_name = "church/create_nursery.html"
    form_class = nurseryForm

    def post(self, request,*args, **kwargs):
        if request.is_ajax:
            form = self.form_class(request.POST)
            if form.is_valid():
                form.save()
                mensaje = 'registrado correctamente!'
                error = 'No hay error!'
                response = JsonResponse({'mensaje':mensaje,'error':error})
                print(mensaje)
                response.status_code = 201
                return response
            else:
                mensaje = f'{self.model.__name__} no se ha podido registrar!'
                error = form.errors
                print(error)
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return reverse_lazy('childs:initial_diagnostic')

class list_church(TemplateView):
    template_name = "church/list_church.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_municipality_id':
                data = []
                for i in municipality.objects.filter(id_state=request.POST['id']):
                    data.append({'id': i.id_municipality, 'name': i.name_municipality})
            elif action == 'search_parish_id':
                data = []
                for i in parish.objects.filter(id_municipality=request.POST['id']):
                    print(i)
                    data.append({'id': i.id_parish, 'name': i.name_parish})
            elif action == 'search_church':
                id = request.POST['id']
                data = []
                k = 0
                for i in parish.objects.raw('SELECT * FROM homes_sector, homes_community, homes_parish, church_church,church_church_sector where homes_parish.id_parish=%s AND homes_parish.id_parish=homes_community.id_parish_id AND homes_community.id_community=homes_sector.id_community_id AND homes_sector.id_sector=church_church_sector.sector_id',[id]):
                    if k == 0:
                        data.append({'id': i.church, 'name': i.name_church, 'direction': i.direction})
                        j = i.church
                        k+=1
                    elif j != i.church:
                        j = i.church
                        data.append({'id': i.church, 'name': i.name_church, 'direction': i.direction})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form'] = churchForm()
        return context


class delete_church(DeleteView):
    model = church
    template_name = "church/delete_church.html"

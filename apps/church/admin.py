from django.contrib import admin

from apps.church.models import church, church_group

# Register your models here.
admin.site.register(church)
admin.site.register(church_group)

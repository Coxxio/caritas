from django.forms import *
from .models import *
from apps.homes.models import parish
from apps.church.models import *
from apps.childs.models import nursery


class groupForm(Form):
    parish = ModelChoiceField(queryset=parish.objects.filter(id_municipality=309),
    widget=Select(attrs={
        'class':'form-select',
        'onchange':'search_church();'
    }))

    church = ModelChoiceField(queryset=church.objects.none(),
    widget=Select(attrs={
        'class':'form-select',
        'onchange':'search_group();'
    }))


class churchForm(Form):

    church = ModelChoiceField(queryset=church.objects.none(), 
    widget=Select(attrs={
        'class':'form-select',
        'id':'church_nursery'
    },
    ))

    church_group = ModelChoiceField(queryset=church_group.objects.none(),
    widget=Select(attrs={
        'class':'form-select'
    },
    ))


class nursery_groupForm(Form):

    church_group = ModelChoiceField(queryset=church_group.objects.none(),
    widget=Select(attrs={
        'class':'form-select',
        'onchange':'search_nursery()'
    },
    ))

    nursery = ModelChoiceField(queryset=nursery.objects.none(),
    widget=Select(attrs={
        'class':'form-select',
        'onchange':'search_childs();'
    })
    )

class loteForm(ModelForm):
    """Form definition for lote."""

    class Meta:
        """Meta definition for loteform."""

        model = lote
        fields = ['lote_number', 'stock', 'total', 'data_supplie', 'date_exp']
        labels = {
            'lote_number':'Numero de Lote',
            'data_supplie':'Tipo de Insumo'
        }
        widgets = {
            'lote_number': TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Numero de Lote del Insumo'
                }
            ),
            'stock': NumberInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Cantidad',
                    'onchange':'multi();',
                    'min':'1'
                }
            ),
            'total': NumberInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Total Disponible',
                    'value':'',
                    'readonly':''
                },
            ),
            'data_supplie': Select(
                attrs={
                    'class':'form-control',
                }
            ),
            'date_exp': DateInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'DD/MM/YYYY'
                }
            ),
        }


class data_supplieForm(ModelForm):
    """Form definition for data_supplie."""

    class Meta:
        """Meta definition for data_supplieform."""

        model = data_supplie
        fields = ['type_supplie', 'category']
        widgets = {
            'type_supplie': TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Nombre del Insumo'
                }
            ),
            'category': TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Categoria del Insumo'
                }
            )
        }

class nurseryForm(ModelForm):
    """Form definition for nursery."""

    class Meta:
        """Meta definition for nurseryform."""

        model = nursery
        fields = (
            'name_nursery',
            'start_date',
            'end_date',
            'status',
            'church_group'
        )

class supplies_churchForm(ModelForm):
    """Form definition for supplies_church."""

    class Meta:
        """Meta definition for supplies_churchform."""

        model = supplies_church
        fields = (
            'group',
            'lote',
            'stock',
            'total',
        )
        labels = {
            'group': 'Grupo Parroquial'
        }
        widgets = {
            'group': Select(
                attrs={
                    'class':'form-select'
                }
            ),
            'lote': Select(
                attrs={
                    'class':'form-select'
                }
            ),
            'stock': NumberInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Cantidad',
                    'onchange':'multi();',
                    'min':'1'

                }
            ),
            'total': NumberInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Total Disponible',
                    'value':'',
                    'readonly':''
                },
            ),
        }

class distributeForm(ModelForm):
    """Form definition for distribute."""
    supplies_church = ModelChoiceField(queryset=supplies_church.objects.exclude(total=0),
    widget=Select(attrs={
        'class':'form-select',
    }
    )
    )

    class Meta:
        """Meta definition for distributeform."""

        model = distribute
        fields = (
            'data_weighing',
            'nursery',
            'supplies_church',
            'stock',
            'status'
        )
        labels ={
            'data_weighing':'Pesaje',
            'nursery':'Vivero',
            'supplies_church':'Suministro',
            'Stock':'Cantidad de insumos',
        }
        widgets = {
            'data_weighing': Select(
                attrs={
                    'class':'form-select'
                }
            ),
            'nursery': Select(
                attrs={
                    'class':'form-select'
                }
            ),
            'stock': NumberInput(
                attrs={
                    'class':'form-control'
                }
            )
        }

from django.urls import path
from .views import *

urlpatterns = [
    path('list-supplies',list_supplies.as_view(), name = 'list_supplies'),
    path('delegate-supplies',delegate_supplies.as_view(), name = 'delegate_supplies'),
    path('delivery-supplies',delivery_supplies.as_view(), name = 'delivery_supplies'),
    path('list-nursery',list_nursery.as_view(), name = 'list_nursery'),
    path('inform/<int:pk>',informView.as_view(), name = 'inform'),
    path('delete-nursery/<int:pk>',nurseryDeleteView.as_view(), name = 'delete_nursery'),
    path('distribute-supplies',distribute_supplies.as_view(), name = 'distribute_supplies'),
    path('create-lote',loteCreateView.as_view(), name = 'create_lote'),
    path('create-supplie',supplieCreateView.as_view(), name = 'create_supplie'),

]
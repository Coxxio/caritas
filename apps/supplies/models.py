from django.db import models
from apps.church.models import church_group
from apps.childs.models import datos_pesaje,jefe_familiar,nursery

class data_supplie(models.Model):
    """Model definition for data_supplies."""

    id_data_supplies = models.AutoField(primary_key=True)
    type_supplie = models.CharField('Tipo de insumo', max_length=50)
    category = models.CharField('Categoria del insumo', max_length=50,null=True)

    class Meta:
        """Meta definition for datos_insumos."""

        verbose_name = 'datos_insumos'
        verbose_name_plural = 'datos_insumos'

    def __str__(self):
        """Unicode representation of datos_insumos."""
        return self.type_supplie

class lote(models.Model):
    """Model definition for lote."""

    lote_number = models.CharField(primary_key=True,max_length=20)
    stock = models.IntegerField('Cajas de insumos del lote')
    total = models.IntegerField('Total de insumos del lote disponibles',default=0)
    data_supplie = models.ForeignKey(data_supplie, on_delete=models.CASCADE)
    date_exp = models.DateField('Fecha de Vencimiento', auto_now=False, auto_now_add=False)

        
    class Meta:
        """Meta definition for lote."""

        verbose_name = 'lote'
        verbose_name_plural = 'lotes'

    def __str__(self):
        """Unicode representation of lote."""
        return self.lote_number


class supplies_church(models.Model):
    """Model definition for insumo_parroquia."""

    supplies_church = models.AutoField(primary_key=True)
    group = models.ForeignKey(church_group, on_delete=models.CASCADE)
    lote = models.ForeignKey(lote, on_delete=models.CASCADE)
    stock = models.IntegerField('Cajas de insumos del lote disponibles')
    total = models.IntegerField('Total de insumos del lote disponibles',default=0)
    date = models.DateField('Fecha de entrega insumos', auto_now_add=True)

    class Meta:
        """Meta definition for insumo_parroquia."""

        verbose_name = 'insumo_parroquia'
        verbose_name_plural = 'insumo_parroquias'

    def __str__(self):
        """Unicode representation of insumo_parroquia."""
        return '{0} Disponible({1})'.format(self.lote.data_supplie.type_supplie, self.lote.total)


class distribute(models.Model):
    """Model definition for distribute."""

    id_distribute = models.AutoField(primary_key=True)
    data_weighing = models.ForeignKey(datos_pesaje, on_delete=models.CASCADE)
    nursery = models.ForeignKey(nursery, on_delete=models.CASCADE)
    supplies_church = models.ForeignKey(supplies_church, on_delete=models.CASCADE)
    stock = models.IntegerField('Cantidad de insumos suministrados')
    status = models.BooleanField(default=False)


    class Meta:
        """Meta definition for reparticion."""

        verbose_name = 'reparticion'
        verbose_name_plural = 'reparticiones'

class distribute_supplies(models.Model):
    """Model definition for entrega_insumo."""

    distribute_supplies = models.OneToOneField(distribute, on_delete=models.CASCADE)
    date_retirement = models.DateField('Fecha del retiro de los insumos', auto_now_add=False)
    representante = models.ForeignKey(jefe_familiar, on_delete=models.CASCADE)

    class Meta:
        """Meta definition for entrega_insumo."""

        verbose_name = 'entrega_insumo'
        verbose_name_plural = 'entrega_insumos'



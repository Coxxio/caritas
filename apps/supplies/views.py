from django.views.generic import CreateView, ListView, TemplateView, DeleteView
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse_lazy
from django.http import JsonResponse
from datetime import date, timedelta
import math

from .forms import *
from .models import *
from apps.homes.models import *
from apps.homes.forms import *
from apps.church.forms import church_groupForm
from apps.childs.models import grupo_familiar, person

# Create your views here.


class list_supplies(ListView):
    model = lote
    template_name = "supplies/list_supplies.html"
    queryset = lote.objects.exclude(stock=0)
    

    def post(self, request, *args, **kwargs):
        action = request.POST['action']
        data = {}
        try:
            if action == 'search_church':
                id = request.POST['id']
                data = []
                for i in church.objects.filter(parish=id):
                    data.append({'id': i.church, 'name': i.name_church})
            elif action == 'search_group':
                id = request.POST['id']
                data = []
                for i in church_group.objects.filter(church=id):
                    data.append({'id': i.church_group, 'name': i.name_church_group})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)


class list_nursery(TemplateView):
    template_name = "supplies/list_nursery.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_municipality_id':
                data = []
                for i in municipality.objects.filter(id_state=request.POST['id']):
                    data.append({'id': i.id_municipality, 'name': i.name_municipality})
            elif action == 'search_parish_id':
                data = []
                for i in parish.objects.filter(id_municipality=request.POST['id']):
                    print(i)
                    data.append({'id': i.id_parish, 'name': i.name_parish})
            elif action == 'search_church':
                id = request.POST['id']
                data = []
                k = 0
                for i in parish.objects.raw('SELECT * FROM homes_sector, homes_community, homes_parish, church_church where homes_parish.id_parish=%s AND homes_parish.id_parish=homes_community.id_parish_id AND homes_community.id_community=homes_sector.id_community_id',[id]):
                    if k == 0:
                        data.append({'id': i.church, 'name': i.name_church})
                        j = i
                        k+=1
                    elif j == i:
                        pass
                    else:
                        data.append({'id': i.church, 'name': i.name_church})
            elif action == 'search_group':
                data = []
                for i in church_group.objects.filter(church=request.POST['id']):
                    data.append({'id': i.church_group, 'name': i.name_church_group})
            elif action == 'search_nursery':
                data = []
                for i in nursery.objects.filter(church_group=request.POST['id']):
                    j = datos_pesaje.objects.filter(nursery=i.nursery).count()
                    data.append({'id': i.nursery, 'name': i.name_nursery, 'start':i.start_date, 'end':i.end_date,'integrants':j,'status':i.status})
            
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form_church'] = churchForm()
        context['form'] = nurseryForm()
        return context

class nurseryDeleteView(DeleteView):
    model = nursery
    template_name = "supplies/delete_nursery.html"
    success_url = 'supplies:list_nursery'

    def post(self,request,pk,*args, **kwargs):
        object = nursery.objects.get(nursery=pk)
        object.status = False
        object.save()
        return self.success_url


class loteCreateView(CreateView):
    model = lote
    template_name = "supplies/create_lote.html"
    form_class = loteForm

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST)
            if form.is_valid():
                form.save()
                mensaje = f'{self.model.__name__} registrado correctamente!'
                error = 'No hay error!'
                response = JsonResponse({'mensaje':mensaje,'error':error})
                response.status_code = 201
                return response
            else:
                mensaje = f'{self.model.__name__} no se ha podido registrar!'
                error = form.errors
                print(error)
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('supplies:list_supplies')
    


class informView(TemplateView):
    template_name = "supplies/inform_nursery.html"


    def get_context_data(self,pk, **kwargs):
        now = date.today()
        five_years = now - timedelta(days=1826)
        six_month = now - timedelta(days=180)
        childs = 0
        childsM = 0
        childsF = 0
        adults = 0
        lactantes = 0
        lactantesM = 0
        lactantesF = 0
        z_score_0 = 0
        z_score_1 = 0
        z_score_2 = 0
        z_score_3 = 0
        context = super().get_context_data(**kwargs)
        context['nursery'] = nursery.objects.get(nursery=pk)
        nurs = nursery.objects.get(nursery=pk)
        dias = nurs.end_date-nurs.start_date
        weeks = dias.days/7
        context['duration'] = round(weeks)
        context['total'] = datos_pesaje.objects.filter(nursery=pk).count()
        for i in datos_pesaje.objects.filter(nursery=pk,accepted=True):
            if i.z_score == -1:
                z_score_1 += 1
            elif i.z_score == -2:
                z_score_2 += 1
            elif i.z_score == -3:
                z_score_3 += 1
            elif i.z_score == 0:
                z_score_0 += 1
            if i.familiar.familiar.birth_date < six_month:
                if i.familiar.familiar.gener == "M":
                    childsM += 1
                elif i.familiar.familiar.gener == "F":
                    childsF += 1
                childs += 1
            elif i.familiar.familiar.birth_date > six_month:
                if i.familiar.familiar.gener == "M":
                    lactantesM += 1
                elif i.familiar.familiar.gener == "F":
                    lactantesF += 1
                lactantes +=1
            if i.familiar.familiar.birth_date < five_years:
                adults += 1
        context['data_accepted'] = datos_pesaje.objects.filter(nursery=pk,accepted=True).count()
        context['data_rejected'] = datos_pesaje.objects.filter(nursery=pk,accepted=False).count()
        context['childs'] = childs
        context['childsM'] = childsM
        context['childsF'] = childsF
        context['adults'] = adults
        context['z_score_0'] = z_score_0
        context['z_score_1'] = z_score_1
        context['z_score_2'] = z_score_2
        context['z_score_3'] = z_score_3
        context['pumply_1'] = z_score_1 * (7*round(weeks))
        context['pumply_2'] = z_score_2 * (7*round(weeks))
        context['pumply_3'] = z_score_3 * (7*round(weeks))
        context['box_1'] = math.ceil((z_score_1 * (7*round(weeks)))/150)
        context['box_2'] = math.ceil((z_score_2 * (7*round(weeks)))/150)
        context['box_3'] = math.ceil((z_score_3 * (7*round(weeks)))/150)
        context['lactantes'] = lactantes
        context['lactantesM'] = lactantesM
        context['lactantesF'] = lactantesF
        return context
    
class supplieCreateView(CreateView):
    model = data_supplie
    template_name = "supplies/create_supplies.html"
    form_class = data_supplieForm
    
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST)
            if form.is_valid():
                form.save()
                mensaje = f'{self.model.__name__} registrado correctamente!'
                error = 'No hay error!'
                response = JsonResponse({'mensaje':mensaje,'error':error})
                response.status_code = 201
                return response
            else:
                mensaje = f'{self.model.__name__} no se ha podido registrar!'
                error = form.errors
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('supplies:list_supplies')

class distribute_supplies(TemplateView):
    template_name = "supplies/distribute_supplies.html"
    
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_church_id':
                data = []
                for i in church.objects.filter(parish=request.POST['id']):
                    data.append({'id': i.church, 'name': i.name_church})
            elif action == 'search_group':
                data = []
                for i in church_group.objects.filter(church=request.POST['id']):
                    data.append({'id': i.church_group, 'name': i.name_church_group})
            elif action == 'search_nursery':
                data = []
                for i in nursery.objects.filter(church_group=request.POST['id']):
                    data.append({'id': i.nursery, 'name': i.name_nursery})
            elif action == 'search_childs':
                data = []
                id=request.POST['id']
                for i in person.objects.raw('SELECT * FROM childs_person, childs_grupo_familiar, childs_jefe_familiar, childs_nursery, childs_datos_pesaje WHERE childs_nursery.nursery=childs_datos_pesaje.nursery_id AND childs_datos_pesaje.familiar_id=childs_person.id_person AND childs_grupo_familiar.familiar_id=childs_person.id_person AND childs_grupo_familiar.chief_id=childs_jefe_familiar.chief_id AND childs_datos_pesaje.delivery>0 AND childs_datos_pesaje.accepted=true'):
                    j = person.objects.get(id_person=i.chief_id)
                    data.append({'representante_name':j.name_person,'representante_last_name':j.last_name_person,'representante_dni':j.dni,'id': i.id_person, 'name': i.name_person, 'last_name':i.last_name_person,'relation':i.relationship,'zscore':i.z_score,'delivery':i.delivery,'id_data':i.data_weighing})
            elif action == 'delivery':
                data = []
                i = datos_pesaje.objects.get(data_weighing=request.POST['id'])
                i.delivery -= 1
                i.save()
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = groupForm()
        context['form_nursery'] = nursery_groupForm()
        context['form'] = home_form()
        return context



class delegate_supplies(TemplateView):
    template_name = "supplies/delegate_supplies.html"

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save()
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = supplies_churchForm(request.POST)
            if form.is_valid():
                delegate = supplies_church(
                    group = form.cleaned_data.get('group'),
                    lote = form.cleaned_data.get('lote'),
                    stock = form.cleaned_data.get('stock'),
                )
                delegate.total = delegate.stock * 150
                loteupdate = lote.objects.get(lote_number=delegate.lote)
                loteupdate.stock -= delegate.stock
                loteupdate.total -= delegate.total
                if loteupdate.stock >= 0:
                    delegate.save()
                    loteupdate.save()
                    loteupdate.refresh_from_db()
                    mensaje = f' registrado correctamente!'
                    error = 'No hay error!'
                    response = JsonResponse({'mensaje':mensaje,'error':error})
                    response.status_code = 201
                elif loteupdate.stock < 0:
                    mensaje = 'El valor es mayor a la cantidad disponible'
                    error = "Error"
                    response = JsonResponse({'mensaje':mensaje,'error':error})
                    response.status_code = 301
                return response
            else:
                mensaje = f' no se ha podido registrar!'
                error = form.errors
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('supplies:list_supplies')



    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = supplies_churchForm
        context['form_group'] = groupForm
        return context


class delivery_supplies(TemplateView):
    template_name = "supplies/delivery_supplies.html"

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            data = {}
            try:
                action = request.POST['action']
                id = request.POST['id']
                if action == 'delivery':

                    mensaje = f' registrado correctamente!'
                    error = 'No hay error!'
                    response = JsonResponse({'mensaje':mensaje,'error':error})
                    response.status_code = 201
                    return response
            except Exception as e:
                data['error'] = str(e)
            return JsonResponse(data, safe=False)



    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = distributeForm
        return context
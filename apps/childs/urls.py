from django.urls import path
from .views import *

urlpatterns = [
    path('create-chief-home',createChiefHomeView.as_view(), name = 'create_chief_home'),
    path('initial-diagnostic',initialDiagnosticView.as_view(), name = 'initial_diagnostic'),
    path('initial-diagnostic-embarazada',initialDiagnosticembarazadaView.as_view(), name = 'initial_diagnostic_embarazada'),
    path('care-dates',careDatesView.as_view(),name = 'care_dates'),
    path('list-monitoring',childMonitoringView.as_view(), name = 'list_monitoring'),
    path('list-childs',childListView.as_view(), name = 'list_childs'),
    path('list-person',personListView.as_view(), name = 'list_person'),
    path('list-chief',chiefListView.as_view(), name = 'list_chief'),
    path('create-person',personCreateView.as_view(), name = 'create_person'),
    path('create-person-familiar',personFamiliarCreateView.as_view(), name = 'create_person_familiar'),
    path('create-chief',chiefCreateView.as_view(), name = 'create_chief'),
    path('create-familiar',familiarCreateView.as_view(), name = 'create_familiar'),
]
from django.shortcuts import redirect, render
from django.views.generic import TemplateView, CreateView, ListView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse_lazy
from django.http.response import JsonResponse
from apps.childs.forms import *
from apps.childs.models import *
from apps.homes.forms import *
from apps.homes.models import *

# Create your views here.


class createChiefHomeView(TemplateView):
    template_name = "childs/create_chief_home.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_municipality_id':
                data = []
                for i in municipality.objects.filter(id_state=request.POST['id']):
                    data.append({'id': i.id_municipality, 'name': i.name_municipality})
            elif action == 'search_parish_id':
                data = []
                for i in parish.objects.filter(id_municipality=request.POST['id']):
                    data.append({'id': i.id_parish, 'name': i.name_parish})
            elif action == 'search_community_id':
                data = []
                for i in community.objects.filter(id_parish=request.POST['id']):
                    data.append({'id': i.id_community, 'name': i.name_community})
            elif action == 'search_sector':
                data = []
                for i in sector.objects.filter(id_community=request.POST['id']):
                    data.append({'id': i.id_sector, 'name': i.name_sector})
            elif action == 'search_home_1':
                data = []
                for i in home.objects.filter(id_sector=request.POST['id']):
                    data.append({'id':i.id_home, 'direction':i.direction})
            elif action == 'search_chief':
                data = []
                for i in jefe_familiar.objects.filter(home=request.POST['id']):
                    data.append({'id':i.chief_id, 'name':i.chief.name_person, 'last_name':i.chief.last_name_person, 'dni':i.chief.dni})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form_home'] = ubication_form2()
        return context   


class careDatesView(TemplateView):
    template_name = "childs/care_dates.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save()
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        data = {}
        form = care_dates_form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
        try:
            action = request.POST['action']
            if action == 'search_municipality_id':
                data = []
                for i in municipality.objects.filter(id_state=request.POST['id']):
                    data.append({'id': i.id_municipality, 'name': i.name_municipality})
            elif action == 'search_parish_id':
                data = []
                for i in parish.objects.filter(id_municipality=request.POST['id']):
                    data.append({'id': i.id_parish, 'name': i.name_parish})
            elif action == 'search_community_id':
                data = []
                for i in community.objects.filter(id_parish=request.POST['id']):
                    data.append({'id': i.id_community, 'name': i.name_community})
            elif action == 'search_sector':
                data = []
                for i in sector.objects.filter(id_community=request.POST['id']):
                    data.append({'id': i.id_sector, 'name': i.name_sector})
            elif action == 'search_home_1':
                data = []
                for i in home.objects.filter(id_sector=request.POST['id']):
                    data.append({'id':i.id_home, 'direction':i.direction})
            elif action == 'search_chief':
                data = []
                for i in jefe_familiar.objects.filter(home=request.POST['id']):
                    data.append({'id':i.chief_id, 'name':i.chief.name_person, 'last_name':i.chief.last_name_person, 'dni':i.chief.dni})
            elif action == 'search_child':
                data = []
                for i in grupo_familiar.objects.filter(chief=request.POST['id']):
                    data.append({'id':i.familiar_id, 'name':i.familiar.name_person, 'last_name':i.familiar.last_name_person, 'relation':i.relationship})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form_home'] = chief_search_form()
        context['care_dates'] = care_dates_form()
        return context

class childMonitoringView(TemplateView):
    template_name = "childs/list_monitoring.html"
    
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_municipality_id':
                data = []
                for i in municipality.objects.filter(id_state=request.POST['id']):
                    data.append({'id': i.id_municipality, 'name': i.name_municipality})
            elif action == 'search_parish_id':
                data = []
                for i in parish.objects.filter(id_municipality=request.POST['id']):
                    data.append({'id': i.id_parish, 'name': i.name_parish})
            elif action == 'search_community_id':
                data = []
                for i in community.objects.filter(id_parish=request.POST['id']):
                    data.append({'id': i.id_community, 'name': i.name_community})
            elif action == 'search_sector':
                data = []
                for i in sector.objects.filter(id_community=request.POST['id']):
                    data.append({'id': i.id_sector, 'name': i.name_sector})
            elif action == 'search_home_1':
                data = []
                for i in home.objects.filter(id_sector=request.POST['id']):
                    data.append({'id':i.id_home, 'direction':i.direction})
            elif action == 'search_child':
                data = []
                for i in grupo_familiar.objects.raw('SELECT * FROM childs_person, childs_grupo_familiar, childs_jefe_familiar, homes_home, homes_sector WHERE homes_sector.id_sector=1 AND homes_home.id_sector_id=homes_sector.id_sector AND homes_home.id_home=childs_jefe_familiar.home_id AND childs_grupo_familiar.familiar_id=childs_person.id_person'):
                    print(i)
                    data.append({'chief_name':i.chief.chief.name_person,'chief_last':i.chief.chief.last_name_person,'id':i.familiar_id, 'name':i.name_person, 'last_name':i.familiar.last_name_person, 'relation':i.relationship})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form'] = home_form()
        return context

class personListView(ListView):
    model = person
    template_name = "childs/list_person.html"

class chiefListView(ListView):
    model = jefe_familiar
    template_name = "childs/list_chief.html"

class childListView(TemplateView):
    template_name = "childs/list_childs.html"
    
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_municipality_id':
                data = []
                for i in municipality.objects.filter(id_state=request.POST['id']):
                    data.append({'id': i.id_municipality, 'name': i.name_municipality})
            elif action == 'search_parish_id':
                data = []
                for i in parish.objects.filter(id_municipality=request.POST['id']):
                    data.append({'id': i.id_parish, 'name': i.name_parish})
            elif action == 'search_community_id':
                data = []
                for i in community.objects.filter(id_parish=request.POST['id']):
                    data.append({'id': i.id_community, 'name': i.name_community})
            elif action == 'search_sector':
                data = []
                for i in sector.objects.filter(id_community=request.POST['id']):
                    data.append({'id': i.id_sector, 'name': i.name_sector})
            elif action == 'search_home_1':
                data = []
                for i in home.objects.filter(id_sector=request.POST['id']):
                    data.append({'id':i.id_home, 'direction':i.direction})
            elif action == 'search_child':
                data = []
                for i in grupo_familiar.objects.raw('SELECT * FROM childs_person, childs_grupo_familiar, childs_jefe_familiar, homes_home, homes_sector WHERE homes_sector.id_sector=1 AND homes_home.id_sector_id=homes_sector.id_sector AND homes_home.id_home=childs_jefe_familiar.home_id AND childs_grupo_familiar.familiar_id=childs_person.id_person'):
                    print(i)
                    data.append({'chief_name':i.chief.chief.name_person,'chief_last':i.chief.chief.last_name_person,'id':i.familiar_id, 'name':i.name_person, 'last_name':i.familiar.last_name_person, 'relation':i.relationship})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form'] = home_form()
        return context

class chiefCreateView(CreateView):
    model = jefe_familiar
    template_name = "childs/create_chief.html"
    form_class = chiefForm

    def post(self, request,*args, **kwargs):
        if request.is_ajax:
            form = self.form_class(request.POST)
            if form.is_valid():
                form.save()
                mensaje = f'{self.model.__name__} registrado correctamente!'
                error = 'No hay error!'
                response = JsonResponse({'mensaje':mensaje,'error':error})
                response.status_code = 201
                return response
            else:
                mensaje = f'{self.model.__name__} no se ha podido registrar!'
                error = form.errors
                print(error)
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('childs:initial_diagnostic')

class familiarCreateView(CreateView):
    model = grupo_familiar
    template_name = "childs/create_familiar.html"
    form_class = familiarForm

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST)
            if form.is_valid():
                form.save()
                mensaje = f'{self.model.__name__} registrado correctamente!'
                error = 'No hay error!'
                response = JsonResponse({'mensaje':mensaje,'error':error})
                response.status_code = 201
                return response
            else:
                mensaje = f'{self.model.__name__} no se ha podido registrar!'
                error = form.errors
                print(error)
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('childs:initial_diagnostic')

class personCreateView(CreateView):
    model = person
    template_name = "childs/create_person.html"
    form_class = personForm

    def post(self, request,*args, **kwargs):
        if request.is_ajax:
            form = self.form_class(request.POST)
            if form.is_valid():
                new_chief = person(
                    id_person=form.cleaned_data.get('dni'),
                    name_person=form.cleaned_data.get('name_person'),
                    last_name_person=form.cleaned_data.get('last_name_person'),
                    dni=form.cleaned_data.get('dni'),
                    gener=form.cleaned_data.get('gener'),
                    birth_date=form.cleaned_data.get('birth_date'),
                )
                new_chief.save()
                mensaje = f'{new_chief.name_person} registrado correctamente!'
                error = 'No hay error!'
                response = JsonResponse({'mensaje':mensaje,'error':error})
                response.status_code = 201
                return response
            else:
                mensaje = f'{self.model.__name__} no se ha podido registrar!'
                error = form.errors
                print(error)
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return reverse_lazy('childs:initial_diagnostic')

class personFamiliarCreateView(CreateView):
    model = person
    template_name = "childs/create_person_familiar.html"
    form_class = personFamiliarForm

    def post(self, request,*args, **kwargs):
        if request.is_ajax:
            form = self.form_class(request.POST)
            if form.is_valid():
                new_chief = person(
                    id_person=form.cleaned_data.get('dni'),
                    name_person=form.cleaned_data.get('name_person'),
                    last_name_person=form.cleaned_data.get('last_name_person'),
                    dni=form.cleaned_data.get('dni'),
                    gener=form.cleaned_data.get('gener'),
                    birth_date=form.cleaned_data.get('birth_date'),
                )

                new_chief.save(commit=False)
                mensaje = f'{new_chief.name_person} registrado correctamente!'
                error = 'No hay error!'
                response = JsonResponse({'mensaje':mensaje,'error':error})
                response.status_code = 201
                return response
            else:
                mensaje = f'{self.model.__name__} no se ha podido registrar!'
                error = form.errors
                print(error)
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return reverse_lazy('childs:initial_diagnostic')

class initialDiagnosticView(TemplateView):
    template_name = "childs/initial_diagnostic.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save()
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        data = {}
        form = data_weighing_form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('childs:care_dates')
        try:
            action = request.POST['action']
            if action == 'search_child':
                chief_id=request.POST['id']
                data = []
                for i in grupo_familiar.objects.filter(chief=chief_id):
                    data.append({'id':i.familiar_id, 'name':i.familiar.name_person, 'last_name':i.familiar.last_name_person, 'relation':i.relationship,})
                for j in church.objects.raw('SELECT * FROM childs_nursery, church_church_group, church_church,church_church_sector, childs_jefe_familiar, homes_home, homes_sector WHERE childs_nursery.status=true AND childs_nursery.church_group_id=church_church_group.church_group AND church_church_group.church_group=church_church.church AND church_church_sector.church_id=church_church.church AND church_church_sector.sector_id=homes_sector.id_sector AND childs_jefe_familiar.home_id=homes_home.id_home AND homes_home.id_sector_id=homes_sector.id_sector AND homes_home.id_sector_id=church_church_sector.sector_id AND childs_jefe_familiar.chief_id=%s',[chief_id]):
                    data.append({'id_nursery':j.nursery, 'name_nursery':j.name_nursery})
            elif action == 'search_age':
                data = []
                i = person.objects.get(id_person=request.POST['id'])
                print(i.birth_date)
                data.append({'age':i.birth_date})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_home'] = chief_search_form()
        context['weighing_form'] = data_weighing_form()
        return context


class initialDiagnosticembarazadaView(TemplateView):
    template_name = "childs/initial_diagnostic_embarazada.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save()
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        data = {}
        form = data_weighing_form_embarazada(request.POST)
        if form.is_valid():
            form.save()
            return redirect('childs:list_childs')
        else:
            print(form.errors)
        try:
            action = request.POST['action']
            if action == 'search_municipality_id':
                data = []
                for i in municipality.objects.filter(id_state=request.POST['id']):
                    data.append({'id': i.id_municipality, 'name': i.name_municipality})
            elif action == 'search_parish_id':
                data = []
                for i in parish.objects.filter(id_municipality=request.POST['id']):
                    data.append({'id': i.id_parish, 'name': i.name_parish})
            elif action == 'search_community_id':
                data = []
                for i in community.objects.filter(id_parish=request.POST['id']):
                    data.append({'id': i.id_community, 'name': i.name_community})
            elif action == 'search_sector':
                data = []
                for i in sector.objects.filter(id_community=request.POST['id']):
                    data.append({'id': i.id_sector, 'name': i.name_sector})
            elif action == 'search_home_1':
                data = []
                for i in home.objects.filter(id_sector=request.POST['id']):
                    data.append({'id':i.id_home, 'direction':i.direction})
            elif action == 'search_chief':
                data = []
                for i in jefe_familiar.objects.filter(home=request.POST['id']):
                    if i.chief.gener == 'F':
                        data.append({'id':i.chief_id, 'name':i.chief.name_person, 'last_name':i.chief.last_name_person, 'dni':i.chief.dni})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form_home'] = ubication_form2()
        context['weighing_form'] = data_weighing_form_embarazada()
        return context
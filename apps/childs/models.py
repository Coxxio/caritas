from django.db import models
from apps.homes.models import home
from apps.church.models import church_group, church

class person(models.Model):
    """Model definition for persona."""

    MADUREZ = [('Niño', 'Niño'), ('Adulto','Adulto')]

    id_person = models.CharField(primary_key=True,max_length=30)
    dni = models.CharField('Cedula de persona', max_length=30, blank=True, null=False, unique=True)
    name_person = models.CharField('Nombre de la persona', max_length=50, blank=False, null=False)
    last_name_person = models.CharField('Apellido de la persona', max_length=50, blank=False, null=False)
    gener = models.CharField('Genero de la persona', max_length=10, blank=False, null=False)
    category = models.CharField('Madurez de la persona', max_length=10, blank=False, null=False, default='Niño', choices=MADUREZ)
    birth_date = models.DateField('Fecha de nacimiento de la persona', auto_now=False, auto_now_add=False)

    class Meta:
        """Meta definition for persona."""

        verbose_name = 'persona'
        verbose_name_plural = 'personas'

    def __str__(self):
        """Unicode representation of persona."""
        return '{0} {1}  C.I.:{2}'.format(self.name_person, self.last_name_person, self.dni)

class jefe_familiar(models.Model):
    """Model definition for jefe_familiar."""

    PROPIEDAD = [(None,'Selecione una opcion'),('Propietario','Propietario'), ('Alquilado','Alquilado'), ('Invasión','Invasión')]

    chief = models.OneToOneField(person, on_delete=models.CASCADE,primary_key=True)
    phone_number = models.CharField('Telefono del jefe familiar', max_length=20)
    property = models.CharField('Titulo de la Vivienda', max_length=20,default='Propietario',choices=PROPIEDAD)
    home = models.ForeignKey(home, on_delete=models.CASCADE)

    class Meta:
        """Meta definition for jefe_familiar."""

        verbose_name = 'jefe_familiar'
        verbose_name_plural = 'jefes_familiares'

    def __str__(self):
        """Unicode representation of jefe_familiar."""
        return '{0} {1}  C.I.:{2}'.format(self.chief.name_person, self.chief.last_name_person, self.chief.dni)

class grupo_familiar(models.Model):
    """Model definition for grupo_familiar."""

    RELATION = [(None,'Selecione una opcion'),('Hijo/a','Hijo/a'), ('Sobrino/a','Sobrino/a'), ('Hermano/a','Hermano/a'), ('Primo/a','Primo/a'),('Nieto/a','Nieto/a')]


    familiar = models.OneToOneField(person, on_delete=models.CASCADE,primary_key=True)
    relationship = models.CharField('Parentesco', max_length=20,blank=False,null=False,choices=RELATION)
    chief = models.ForeignKey(jefe_familiar, on_delete=models.CASCADE)

    class Meta:
        """Meta definition for grupo_familiar."""

        verbose_name = 'grupo_familiar'
        verbose_name_plural = 'grupo_familiares'

    def __str__(self):
        """Unicode representation of jefe_familiar."""
        return '{0} {1}  ({2})'.format(self.familiar.name_person, self.familiar.last_name_person,self.relationship)


class datos_cuidado(models.Model):
    """Model definition for datos_cuidado."""

    familiar = models.OneToOneField(grupo_familiar, on_delete=models.CASCADE)
    breastmilk = models.CharField('El niño consume leche materna', max_length=10,blank=False,null=False)
    breastmilk_intake = models.IntegerField('Veces que consime leche materna al dia')
    bottle = models.CharField('Consumo de tetero del niño', max_length=10, blank=False,null=False)
    solid_food = models.CharField('Consumo de alimentos solidos del niño', max_length=10,blank=False,null=False)
    boiled_water = models.CharField('Agua para el consumo es hervida', max_length=10,blank=False,null=False)
    filtred_water = models.CharField('Agua para el consumo es filtrada', max_length=10,blank=False,null=False)
    purified_water = models.CharField('Agua para el consumo es filtrada', max_length=10,blank=False,null=False)
    flu_cough = models.CharField('Manifesto Gripe o tos la ultima semana', max_length=10,blank=False,null=False)
    fever = models.CharField('Manifesto fiebre la ultima semana', max_length=10,blank=False,null=False)
    diarrhea = models.CharField('Manifesto diarrea la ultima semana', max_length=10,blank=False,null=False)
    vomit = models.CharField('Presento vomito la ultima semana', max_length=10,blank=False,null=False)
    dehydrataded = models.CharField('Presento deshidratacion la ultima semana', max_length=10,blank=False,null=False)
    malaria = models.CharField('Presento paludismo la ultima semana', max_length=10, blank=False,null=False)

    class Meta:
        """Meta definition for datos_cuidado."""

        verbose_name = 'datos_cuidado'
        verbose_name_plural = 'datos_cuidados'

    def __str__(self):
        """Unicode representation of datos_cuidado."""
        return self.familiar

class nursery(models.Model):
    """Model definition for nursery."""

    nursery = models.AutoField(primary_key=True)
    name_nursery = models.CharField('Nombre del vivero',max_length=50,default='Vivero')
    start_date = models.DateField('Fecha de inicio del vivero', auto_now=False, auto_now_add=False)
    end_date = models.DateField('Fecha final del vivero', auto_now=False, auto_now_add=False)
    status = models.BooleanField('Estado del Viviero',default=True)
    church_group = models.ForeignKey(church_group, on_delete=models.CASCADE)

    class Meta:
        """Meta definition for vivero."""

        verbose_name = 'vivero'
        verbose_name_plural = 'viveros'

    def __str__(self):
        """Unicode representation of vivero."""
        return self.name_nursery

class datos_pesaje(models.Model):
    """Model definition for datos_pesaje."""

    data_weighing = models.AutoField(primary_key=True)
    height = models.FloatField('Altura del niño',blank=False,null=False)
    weight = models.FloatField('Peso del niño',blank=False,null=False)
    c_arm = models.FloatField('Circuferencia del brazo del niño',blank=False,null=False)
    z_score = models.IntegerField('Indice de desnutricion del niño')
    accepted = models.BooleanField(default=True)
    delivery = models.IntegerField(default=3)
    nursery = models.ForeignKey(nursery, on_delete=models.CASCADE)
    familiar = models.ForeignKey(grupo_familiar, on_delete=models.CASCADE,max_length=30)

    class Meta:
        """Meta definition for datos_pesaje."""

        verbose_name = 'datos_pesaje'
        verbose_name_plural = 'datos_pesajes'

    def __str__(self):
        """Unicode representation of datos_pesaje."""
        return self.familiar.familiar.name_person

class datos_pesaje_embarazada(models.Model):
    """Model definition for datos_pesaje."""

    data_weighing = models.AutoField(primary_key=True)
    height = models.FloatField('Altura de la Embarazada',blank=False,null=False)
    weight = models.FloatField('Peso de la Embarazada',blank=False,null=False)
    c_arm = models.FloatField('Circuferencia del brazo de la Embarazada',blank=False,null=False)
    z_score = models.IntegerField('Indice de desnutricion de la Embarazada')
    gestation = models.IntegerField('Semana de Gestacion')
    accepted = models.BooleanField(default=True)
    nursery = models.ForeignKey(nursery, on_delete=models.CASCADE)
    familiar = models.ForeignKey(jefe_familiar, on_delete=models.CASCADE,max_length=30)

    class Meta:
        """Meta definition for datos_pesaje."""

        verbose_name = 'datos_pesaje'
        verbose_name_plural = 'datos_pesajes'

    def __str__(self):
        """Unicode representation of datos_pesaje."""
        return self.familiar

class priest(models.Model):
    """Model definition for parroco."""

    priest = models.CharField(primary_key=True,max_length=10)
    status = models.BooleanField('Estado del parroco en la iglesia', default=True)
    start_date = models.DateField('Fecha de inicio del parroco en la igleisa',auto_now_add=True)
    person = models.ForeignKey(person, on_delete=models.CASCADE)
    church = models.ForeignKey(church, on_delete=models.CASCADE)

    class Meta:
        """Meta definition for parroco."""

        verbose_name = 'parroco'
        verbose_name_plural = 'parrocos'

    def __str__(self):
        """Unicode representation of parroco."""
        return '{0},{1}'.format(self.persona.nombre_persona, self.persona.apellido_persona)

class integrant_church_group(models.Model):
    """Model definition for integrante_grupo_parroquial."""

    integrant = models.AutoField(primary_key=True)
    status = models.BooleanField('Estado del integrante en el grupo', default=True)
    start_date = models.DateField('Fecha de inicio', auto_now_add=False)
    rank = models.CharField('Cargo del integrando dentro del grupo', max_length=20, blank=True, null=True)
    person = models.ForeignKey(person, on_delete=models.CASCADE)
    church_group = models.ForeignKey(church_group, on_delete=models.CASCADE)

    class Meta:
        """Meta definition for integrante_grupo_parroquial."""

        verbose_name = 'integrante_grupo_parroquial'
        verbose_name_plural = 'integrante_grupo_parroquiales'

    def __str__(self):
        """Unicode representation of integrante_grupo_parroquial."""
        return '{0} {1}'.format(self.person.name_person, self.person.last_name_person)


from .models import *
from django.forms import *
from apps.homes.models import home



class ubication_form2(Form):

    home = ModelChoiceField(queryset=home.objects.none(), 
    widget=Select(attrs={
        'class':'form-select',
        'onchange':'search_chief();'
    },
    ))

    chief = ModelChoiceField(queryset=jefe_familiar.objects.all(),
    widget=Select(attrs={
        'class':'form-select',
        'onchange':'search_child()'
    },
    ))
class ubication_form3(Form):

    home = ModelChoiceField(queryset=home.objects.none(), 
    widget=Select(attrs={
        'class':'form-select',
        'id':'home_pregnant'
    },
    ))

class chief_search_form(Form):
    chief = ModelChoiceField(queryset=jefe_familiar.objects.all(),
    widget=Select(attrs={
        'class':'form-select',
        'id':'chief_search',
        'onchange':'child_familiar()'
    },
    ))

class data_weighing_form(ModelForm):
    """Form definition for data_weighing."""
    
    class Meta:
        """Meta definition for data_weighing_form."""
        model = datos_pesaje
        fields = [
            'height',
            'weight',
            'c_arm',
            'z_score',
            'nursery',
            'accepted',
            'familiar',
            'nursery'
        ]
        widgets = {
            'height' : NumberInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Altura en Cm'
                }
            ),
            'weight' : NumberInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Peso en Kg'
            }),
            'nursery':Select(
                attrs={
                    'class':'form-select',
                }
            ),
            'c_arm' : NumberInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Circunferencia en Cm',
                    'onKeyUp':'score();'
            }),
            'z_score' : NumberInput(
                attrs={
                    'class':'form-control',
                    'readonly':''
            }),
            'nursery' :Select(
                attrs={
                    'class':'form-control',
                    'placeholder':'DD/MM/YYYY'
            }),
            'familiar' :Select(
                attrs={
                    'class':'form-select',
            })
    }

class data_weighing_form_embarazada(ModelForm):
    """Form definition for data_weighing."""
    
    class Meta:
        """Meta definition for data_weighing_form."""
        model = datos_pesaje_embarazada
        fields = [
            'height',
            'weight',
            'c_arm',
            'z_score',
            'nursery',
            'gestation',
            'accepted',
            'familiar',
            'nursery'
        ]
        widgets = {
            'height' : NumberInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Altura en Cm'
                }
            ),
            'weight' : NumberInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Peso en Kg'
            }),
            'nursery':Select(
                attrs={
                    'class':'form-select',
                }
            ),
            'c_arm' : NumberInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Circunferencia en Cm',
                    'onKeyUp':'score();'
            }),
            'z_score' : NumberInput(
                attrs={
                    'class':'form-control',
                    'readonly':''
            }),
            'gestation': NumberInput(
                attrs={
                    'class':'form-control'
                }
            ),
            'nursery' :Select(
                attrs={
                    'class':'form-control',
                    'placeholder':'DD/MM/YYYY'
            }),
            'familiar' :Select(
                attrs={
                    'class':'form-select',
                    'id':'id_chief'
            })
    }

class care_dates_form(ModelForm):
    """Form definition for care_dates."""
    
    class Meta:
        """Meta definition for care_dates_form."""
        CHOICES = [(None,'Selecione una respuesta'),('Si','Si'), ('No','No'), ('Ns/Nr','No sabe/No respondió')]

        model = datos_cuidado
        fields = [
            'breastmilk',
            'breastmilk_intake',
            'bottle',
            'solid_food',
            'boiled_water',
            'filtred_water',
            'purified_water',
            'flu_cough',
            'fever',
            'diarrhea',
            'vomit',
            'dehydrataded',
            'malaria',
            'familiar'
        ]
        widgets = {
            'breastmilk' :Select(attrs={
                'class':'form-select',
            },
            choices=CHOICES
            ),

            'bottle' :Select(attrs={
                'class':'form-select',
            },
            choices=CHOICES
            ),

            'breastmilk_intake' :NumberInput(attrs={
                'class':'form-control',
                
            }),

            'solid_food' :Select(attrs={
                'class':'form-select',
            },
            choices=CHOICES
            ),
            'boiled_water' :Select(attrs={
                'class':'form-select',
            },
            choices=CHOICES
            ),
            'filtred_water' :Select(attrs={
                'class':'form-select',
            },
            choices=CHOICES
            ),
            'purified_water' :Select(attrs={
                'class':'form-select',
            },
            choices=CHOICES
            ),

            'flu_cough' :Select(attrs={
                'class':'form-select',
            },
            choices=CHOICES
            ),
            'fever' :Select(attrs={
                'class':'form-select',
            },
            choices=CHOICES
            ),

            'diarrhea' :Select(attrs={
                'class':'form-select',
            },
            choices=CHOICES
            ),
            'vomit' :Select(attrs={
                'class':'form-select',
            },
            choices=CHOICES
            ),
            'dehydrataded' :Select(attrs={
                'class':'form-select',
            },
            choices=CHOICES
            ),
            'malaria' :Select(attrs={
                'class':'form-select',
            },
            choices=CHOICES
            ),
            'familiar' :Select(attrs={
                    'class':'form-select',
            })
    }
class personForm(ModelForm):
    

    class Meta:
        model = person
        fields = [
            'dni',
            'name_person',
            'last_name_person',
            'gener',
            'birth_date',
            'category'
        ]
        widgets ={
            'dni': NumberInput(
                attrs={
                    'class':'form-control'
                }
            ),
            'name_person': TextInput(
                attrs={
                    'class':'form-control'
                }
            ),
            'last_name_person': TextInput(
                attrs={
                    'class':'form-control'
                }
            ),
            'gener': Select(
                attrs={
                    'class':'form-select',
                },
                choices=[('M','Masculino'),('F','Femenino')]
            ),
            'birth_date': DateInput(
                attrs={
                    'class':'form-control'
                }
            ),
            'category': Select(
                attrs={
                    'class':'form-select',
                },
            )
        }

class personFamiliarForm(ModelForm):
    

    class Meta:
        model = person
        fields = [
            'dni',
            'name_person',
            'last_name_person',
            'gener',
            'birth_date',
            'category'
        ]
        widgets ={
            'dni': TextInput(
                attrs={
                    'class':'form-control invisible',
                    'readonly':'',
                }
            ),
            'name_person': TextInput(
                attrs={
                    'class':'form-control'
                }
            ),
            'last_name_person': TextInput(
                attrs={
                    'class':'form-control'
                }
            ),
            'gener': Select(
                attrs={
                    'class':'form-select'
                },
                choices=[('M','Masculino'),('F','Femenino')]
            ),
            'birth_date': DateInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'DD/MM/AAAA',
                    'onblur':'chief_child();'
                }
            ),
            'category': Select(
                attrs={
                    'class':'form-select invisible',
                }
            )
        }

class chiefForm(ModelForm):
    
    class Meta:
        model = jefe_familiar
        fields = [
            'chief',
            'phone_number',
            'property',
            'home'
        ]
        labels = {
            'chief':'Persona',
            'phone_number':'Numero de Telefono',
            'property':'Titulo de la propiedad',
            'home': 'Vivienda'
        }
        widgets ={
            'chief': Select(
                attrs={
                    'class':'form-select',
                    'id':'chief_search'
                }
            ),
            'phone_number': NumberInput(
                attrs={
                    'class':'form-control'
                }
            ),
            'property':Select(
                attrs={
                    'class':'form-select',
                }
            ),
            
            'home': Select(
                attrs={
                    'class':'form-select',
                    'id':'id_home_chief',
                }
            )
        }

class familiarForm(ModelForm):
    
    class Meta:
        model = grupo_familiar
        fields = [
            'familiar',
            'relationship',
            'chief'
        ]
        labels = {
            'familiar':'Niño',
            'chief':'Representante'
        }
        widgets ={
            'familiar':Select(
                attrs={
                    'class':'form-select',
                    'id':'id_familiar_child',
                    'readonly':''
                }
            ),
            'relationship':Select(
                attrs={
                    'class':'form-select',
                }
            ),
            'chief':Select(
                attrs={
                    'class':'form-select',
                    'id':'chief_familiar',
                    'readonly':''
                }
            )
        }
 


class nurseryForm(ModelForm):
    """Form definition for nursery."""

    class Meta:
        """Meta definition for nurseryform."""

        model = nursery
        fields = ( 
            'name_nursery',
            'start_date',
            'end_date',
            'church_group'
        )
        labels = {
            'church_group':'Grupo Parroquial'
        }
        widgets={
            'name_nursery' : TextInput(
                attrs={
                    'class':'form-control'
                }
            ),
            'start_date': DateInput(
                attrs={
                    'class':'form-control'
                }
            ),
            'end_date': DateInput(
                attrs={
                    'class':'form-control'
                }
            ),
            'church_group': Select(
                attrs={
                    'class':'form-control'
                }
            )
        }


class integrantForm(ModelForm):
    """Form definition for integrant."""
    person = ModelChoiceField(queryset=person.objects.filter(category='Adulto'), 
    widget=Select(attrs={
        'class':'form-select'
    }))

    class Meta:
        """Meta definition for integrantform."""

        model = integrant_church_group
        fields = (
            'status',
            'start_date',
            'rank',
            'person',
            'church_group'
        )
        labels = {
            'church_group':'Grupo Parroquial'
        }
        widgets ={
            'start_date' :DateInput(
                attrs={
                    'class':'form-control',
                }
            ),
            'rank' : TextInput(
                attrs={
                    'class':'form-control'
                },
            ),
            'church_group': Select(
                attrs={
                    'class':'form-select'
                }
            )
        }


from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from apps.childs.models import person
# Create your models here.

class UsuarioManager(BaseUserManager):
    def _create_user(self, username, email, voluntary, password, is_staff, is_superuser, **extra_fields):
        integrant = person.objects.get(id_person=voluntary)
        user = self.model(
            username=username,
            email=email,
            voluntary=integrant,
            is_staff=is_staff,
            is_superuser=is_superuser,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self.db)
        return user

    def create_user(self, username, email, voluntary, is_staff,password=None,**extra_fields):
        return self._create_user(username, email, voluntary, password, is_staff, False,**extra_fields)
    
    def create_superuser(self,username,email, voluntary,password = None,**extra_fields):
        return self._create_user(username, email, voluntary, password, True, True,**extra_fields)

class User(AbstractBaseUser):
    username = models.CharField('Nombre de Usuario', max_length=50,unique=True)
    email = models.EmailField('Correo electronico', max_length=254, unique=True,default='a@a.com')
    voluntary = models.ForeignKey(person, on_delete=models.CASCADE, related_name='Voluntario')
    status = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser= models.BooleanField(default=False)

    objects = UsuarioManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email','voluntary']

    def __str__(self) -> str:
        return f'{self.username}'

    def has_perm(self,perm,obj=None):
        return True

    def has_module_perms(self,app_label):
        return True

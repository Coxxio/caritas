from django.db import models
from django.db.models.query import QuerySet

class state(models.Model):
    """Model definition for state."""

    id_state = models.AutoField(primary_key=True)
    name_state = models.CharField('Nombre del Estado',max_length=30,blank=False, null=False,default=0)

    class Meta:
        """Meta definition for state."""

        verbose_name = 'state'
        verbose_name_plural = 'states'

    def __str__(self):
        """Unicode representation of state."""
        return self.name_state

class municipality(models.Model):
    """Model definition for municipality."""

    id_municipality = models.AutoField(primary_key=True)
    name_municipality = models.CharField('Nombre del Municipio', max_length=100, blank=False,null=False)
    id_state = models.ForeignKey(state, on_delete=models.CASCADE)

    class Meta:
        """Meta definition for municipality."""

        verbose_name = 'municipality'
        verbose_name_plural = 'municipalitys'

    def __str__(self):
        """Unicode representation of municipality."""
        return self.name_municipality

class parish(models.Model):
    """Model definition for parish."""

    id_parish = models.AutoField(primary_key=True)
    name_parish = models.CharField('Nombre de la Parroquia', max_length=50, blank=False,null=False)
    id_municipality = models.ForeignKey(municipality, on_delete=models.CASCADE)

    class Meta:
        """Meta definition for parish."""

        verbose_name = 'parish'
        verbose_name_plural = 'parishs'

    def __str__(self):
        """Unicode representation of parish."""
        return self.name_parish

class community(models.Model):
    """Model definition for community."""

    id_community = models.AutoField(primary_key=True)
    name_community = models.CharField('Nombre de la Comunidad', max_length=50, blank=False,null=False)
    id_parish = models.ForeignKey(parish, on_delete=models.CASCADE)

    class Meta:
        """Meta definition for community."""

        verbose_name = 'Comunidad'
        verbose_name_plural = 'Comunidades'

    def __str__(self):
        """Unicode representation of community."""
        return self.name_community

class sector(models.Model):
    """Model definition for sector."""

    id_sector = models.AutoField(primary_key=True)
    name_sector = models.CharField('Nombre del sector', max_length=50, blank=False,null=False)
    id_community = models.ForeignKey(community, on_delete=models.CASCADE)

    class Meta:
        """Meta definition for sector."""

        verbose_name = 'sector'
        verbose_name_plural = 'sectores'

    def __str__(self):
        """Unicode representation of sector."""
        return self.name_sector

class have_bath(models.Model):
    """Model definition of have_bath"""

    name = models.CharField('Utiencilio Baño',max_length=20)

    class Meta:
        verbose_name = 'have_bath'
        verbose_name_plural = 'have_baths'

    def __str__(self):
        return self.name

class home(models.Model):
    """Model definition for home."""
    
    STATUS=[(None,'Selecione una opcion'),('Bueno','Bueno'), ('Regular','Regular'), ('Malo','Malo')]

    id_home = models.AutoField(primary_key=True)
    direction = models.CharField('Direccion de la Vivienda', max_length=100,blank=False,null=False)
    type_floor = models.CharField('Tipo de piso de la Vivienda', max_length=20)
    status_floor = models.CharField('Estado del piso de la Vivienda', max_length=10,choices=STATUS,default=None)
    type_roof = models.CharField('Tipo de techo de la Vivienda', max_length=50)
    status_roof = models.CharField('Estado de la pared de la Vivienda', max_length=50,choices=STATUS,default=None)
    type_wall = models.CharField('Tipo de la pared de la Vivienda', max_length=50)
    status_wall = models.CharField('Estado de la pared de la Vivienda', max_length=50,choices=STATUS,default=None)
    white_water = models.BooleanField('Servicio de aguas blancas de la Vivienda')
    status_white_water = models.CharField('Estado del servicio de aguas blancas de la Vivienda', max_length=10,choices=STATUS,default=None)
    wastewater = models.BooleanField('Servicio de aguas servidas de la Vivienda')
    status_wastewater = models.CharField('Estado del servicio de aguas servidas de la Vivienda', max_length=10,choices=STATUS,default=None)
    n_rooms = models.IntegerField('Numero de Habitaciones', blank=False, null=False,default=0)
    status_rooms = models.CharField('Estado de las Habitaciones', blank=False, null=False,max_length=20,choices=STATUS,default=None)
    n_bath = models.IntegerField('Numero de Baños', blank=False, null=False)
    have_bath = models.ManyToManyField(have_bath,blank=True)
    kitchen = models.BooleanField('Cocina de la Vivienda')
    state_kitchen = models.CharField('Estado de la Cocina',max_length=50,choices=STATUS,default=None)
    id_sector = models.ForeignKey(sector, on_delete=models.CASCADE,default=1)


    class Meta:
        """Meta definition for home."""

        verbose_name = 'Viviendas'
        verbose_name_plural = 'Viviendass'

    def __str__(self):
        """Unicode representation of home."""
        return self.direction




from django.forms import *
from .models import *

class ubicationForm(Form):
    """ubicationForm definition."""

    state = ModelChoiceField(queryset=state.objects.all(), 
    widget=Select(attrs={
        'class':'form-select'
    }))

    municipality = ModelChoiceField(queryset=municipality.objects.none(), 
    widget=Select(attrs={
        'class':'form-select'
    }))

    parish = ModelChoiceField(queryset=parish.objects.filter(id_municipality=309), 
    widget=Select(attrs={
        'class':'form-select',
        'onchange':'search_parish();'
    }))
    

    community = ModelChoiceField(queryset=community.objects.none(), 
    widget=Select(attrs={
        'class':'form-select',
        'onchange':'search_sector();'
    }))

    sector = ModelChoiceField(queryset=sector.objects.none(), 
    widget=Select(attrs={
        'class':'form-select'
    }))


class communityForm(ModelForm):
    """Form definition for community."""
    id_parish = ModelChoiceField(queryset=parish.objects.filter(id_municipality=309), 
    widget=Select(attrs={
        'class':'form-select'
    }),
    label= 'Parroquia'
    )

    class Meta:
        """Meta definition for communityform."""

        model = community
        fields = ['id_community', 'name_community','id_parish']
        widgets = {
            'name_community': TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Nombre de la Comunidad'
                }
            )
        }

class sectorForm(ModelForm):
    """Form definition for sector."""

    class Meta:
        """Meta definition for sectorform."""

        model = sector
        fields = ['id_sector', 'name_sector','id_community']
        labels = {
            'id_community':'Comunidad'
        }
        widgets = {
            'id_community': Select(
                attrs={
                    'class':'form-select',
                }
            ),
            'name_sector': TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Nombre del Sector'
                }
            )
        }



class home_form(ModelForm):
    """Form definition for home."""
    

    class Meta:
        """Meta definition for homeform."""

        model = home
        fields = [
            'direction',
            'type_floor',
            'status_floor',
            'type_roof',
            'status_roof',
            'type_wall',
            'status_wall',
            'white_water',
            'status_white_water',
            'wastewater',
            'status_wastewater',
            'n_rooms',
            'status_rooms',
            'n_bath',
            'have_bath',
            'kitchen',
            'state_kitchen',
            'id_sector']
        labels = {
            'direction':'Dirección',
            'type_floor':'Tipo de Piso',
            'status_floor':'Estado del Piso',
            'type_roof':'Tipo de Techo',
            'status_roof':'Estado del Techo',
            'type_wall':'Tipo de Pared',
            'status_wall':'Estado de la Pared',
            'white_water':'Servicio de Aguas Blancas',
            'status_white_water':'Estado del Servicio de Aguas Blancas',
            'wastewater':'Aguas Servidas',
            'status_wastewater':'Estado del Servicio de Aguas Servidas',
            'n_rooms':'Numero de habitaciones',
            'status_rooms':'Estado de las habitaciones',
            'n_bath':'Numero de baños',
            'have_bath':'Contenido de baños',
            'kitchen':'Cocina',
            'state_kitchen':'Estado de la cocina',
            'id_sector':'Sector'
            }
        widgets = {
            'direction': TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder': 'Direccion de la vivienda'
                }
            ),
            'type_floor': Select(
                attrs={
                    'class': 'form-select',
                },
                choices=[('Ceramica','Ceramica'), ('Cemento','Cemento'), ('Tierra','Tierra')]
            ),
            'type_wall': Select(
                attrs={
                    'class': 'form-select',
                },
                choices=[('Bloque','Bloque'), ('Madera','Madera'), ('Zinc','Zinc')]
            ),
            'type_roof': Select(
                attrs={
                    'class': 'form-select',
                },
                choices=[('Platabanda','Platabanda'), ('Zinc','Zinc')]
            ),
            'white_water': Select(
                attrs={
                    'class': 'form-select',
                },
                choices=[('1','Si'), ('0','No')]
            ),
            'wastewater': Select(
                attrs={
                    'class': 'form-select',
                },
                choices=[('1','Si'), ('0','No')]
            ),
            'status_floor': Select(
                attrs={
                    'class': 'form-select',
                },
            ),
            'status_wall': Select(
                attrs={
                    'class': 'form-select',
                },
            ),
            'status_roof': Select(
                attrs={
                    'class': 'form-select',
                },
                choices=[('Bueno','Bueno'), ('Regular','Regular'), ('Malo','Malo')]
            ),
            'status_white_water': Select(
                attrs={
                    'class': 'form-select',
                },
                choices=[('Bueno','Bueno'), ('Regular','Regular'), ('Malo','Malo')]
            ),
            'status_wastewater': Select(
                attrs={
                    'class': 'form-select',
                },
                choices=[('Bueno','Bueno'), ('Regular','Regular'), ('Malo','Malo')]
            ),
            'n_rooms': NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder':'Ingrese numero de habitaciones'
                }
            ),
            'status_rooms': Select(
                attrs={
                    'class': 'form-select',
                },
                choices=[('Bueno','Bueno'), ('Regular','Regular'), ('Malo','Malo')]
            ),
            'n_bath': NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder':'Ingrese numero de baños'
                }
            ),
            'have_bath': CheckboxSelectMultiple(
                attrs={
                    'class': 'form-checkbox'
                }
            ),
            'kitchen': Select(
                attrs={
                    'class': 'form-select',
                },
                choices=[('1','Si'), ('0','No')]
            ),
            'state_kitchen': Select(
                attrs={
                    'class': 'form-select',
                },
                choices=[('Bueno','Bueno'), ('Regular','Regular'), ('Malo','Malo')]
            ),
            'id_sector': Select(
                attrs={
                    'class': 'form-select',
                },
            )
        }
        

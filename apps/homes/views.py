from django.http import response
from django.http.response import JsonResponse
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, UpdateView, DeleteView, DetailView, CreateView, ListView
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from apps.childs.models import datos_pesaje, datos_pesaje_embarazada, integrant_church_group, nursery
from apps.homes.forms import *
from apps.homes.models import *

class login_view(TemplateView):
    template_name = 'login.html'

class home_view(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['childs'] = datos_pesaje.objects.all().count()
        context['nursery'] = nursery.objects.all().count()
        context['voluntary'] = integrant_church_group.objects.all().count()
        context['community'] = community.objects.all().count()
        return context

def data(request,*args, **kwargs):
    data = {
        "dataFirst": datos_pesaje.objects.filter(z_score=-1),
        "dataSecond": datos_pesaje.objects.filter(z_score=-2),
        "dataThird": datos_pesaje.objects.filter(z_score=-3),

    }
    return JsonResponse(data)


class communityListView(TemplateView):
    template_name = "homes/list_community.html"
    
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_community_id':
                data = []
                for i in community.objects.filter(id_parish=request.POST['id']):
                    data.append({'id': i.id_community, 'name': i.name_community})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form'] = home_form()
        return context


class sectorListView(TemplateView):
    template_name = "homes/list_sector.html"
    
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_parish':
                data = []
                for i in parish.objects.filter(id_municipality=request.POST['id']):
                    data.append({'id': i.id_parish, 'name': i.name_parish})
            elif action == 'search_community_id':
                data = []
                for i in community.objects.filter(id_parish=request.POST['id']):
                    data.append({'id': i.id_community, 'name': i.name_community})
            elif action == 'search_sector':
                data = []
                for i in sector.objects.filter(id_community=request.POST['id']):
                    data.append({'id': i.id_sector, 'name': i.name_sector})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form'] = home_form()
        return context

class communityCreateView(CreateView):
    model = community
    template_name = "homes/create_community.html"
    form_class = communityForm

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST)
            if form.is_valid():
                form.save()
                mensaje = 'registrado correctamente!'
                error = 'No hay error!'
                response = JsonResponse({'mensaje':mensaje,'error':error})
                response.status_code = 201
                return response
            else:
                mensaje = 'no se ha podido registrar!'
                error = form.errors
                print(error)
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('homes:list_community')

class sectorCreateView(CreateView):
    model = sector
    template_name = "homes/create_sector.html"
    form_class = sectorForm

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST)
            if form.is_valid():
                form.save()
                mensaje = 'registrado correctamente!'
                error = 'No hay error!'
                response = JsonResponse({'mensaje':mensaje,'error':error})
                response.status_code = 201
                return response
            else:
                mensaje = 'no se ha podido registrar!'
                error = form.errors
                print(error)
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('homes:list_sector')
    



class homeCreateView(TemplateView):
    template_name = "homes/create_home.html"
    success_url = reverse_lazy('homes:list_home')
    
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        self.object = form.save()
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        data = {}
        form = home_form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homes:list_home')
        
        try:
            action = request.POST['action']
            if action == 'search_municipality_id':
                data = []
                for i in municipality.objects.filter(id_state=request.POST['id']):
                    data.append({'id': i.id_municipality, 'name': i.name_municipality})
            elif action == 'search_parish_id':
                data = []
                for i in parish.objects.filter(id_municipality=request.POST['id']):
                    data.append({'id': i.id_parish, 'name': i.name_parish})
            elif action == 'search_community_id':
                data = []
                for i in community.objects.filter(id_parish=request.POST['id']):
                    data.append({'id': i.id_community, 'name': i.name_community})
            elif action == 'search_sector':
                data = []
                for i in sector.objects.filter(id_community=request.POST['id']):
                    data.append({'id': i.id_sector, 'name': i.name_sector})
            elif action == 'create-home':
                print('hola')
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form'] = home_form()
        return context


class homeListView(TemplateView):
    template_name = "homes/list_home.html"
    
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_municipality_id':
                data = []
                for i in municipality.objects.filter(id_state=request.POST['id']):
                    data.append({'id': i.id_municipality, 'name': i.name_municipality})
            elif action == 'search_parish_id':
                data = []
                for i in parish.objects.filter(id_municipality=request.POST['id']):
                    data.append({'id': i.id_parish, 'name': i.name_parish})
            elif action == 'search_community_id':
                data = []
                for i in community.objects.filter(id_parish=request.POST['id']):
                    data.append({'id': i.id_community, 'name': i.name_community})
            elif action == 'search_sector':
                data = []
                for i in sector.objects.filter(id_community=request.POST['id']):
                    data.append({'id': i.id_sector, 'name': i.name_sector})
            elif action == 'search_home':
                data = []
                for i in home.objects.filter(id_sector=request.POST['id']):
                    data.append({'id':i.id_home, 'direction':i.direction, 'type_floor':i.type_floor, 'type_wall':i.type_wall,'type_roof':i.type_roof,})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_ubication'] = ubicationForm()
        context['form'] = home_form()
        return context


class homeDetailView(DetailView):
    model = home
    form_class = home_form
    template_name = "homes/detail_home.html"


class homeUpdateView(UpdateView):
    model = home
    form_class = home_form
    template_name = "homes/edit_home.html"
    success_url = reverse_lazy("homes:list_home")



class communityUpdateView(UpdateView):
    model = community
    template_name = "homes/update_community.html"
    form_class = communityForm

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST,instance =self.get_object())
            if form.is_valid():
                form.save()
                mensaje = 'Editado correctamente!'
                error = 'No hay error!'
                response = JsonResponse({'mensaje':mensaje,'error':error})
                response.status_code = 201
                return response
            else:
                mensaje = 'no se ha podido editar!'
                error = form.errors
                print(error)
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('homes:list_community')

class sectorUpdateView(UpdateView):
    model = sector
    template_name = "homes/update_sector.html"
    form_class = sectorForm

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST,instance =self.get_object())
            if form.is_valid():
                form.save()
                mensaje = 'Editado correctamente!'
                error = 'No hay error!'
                response = JsonResponse({'mensaje':mensaje,'error':error})
                response.status_code = 201
                return response
            else:
                mensaje = 'no se ha podido editar!'
                error = form.errors
                print(error)
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('homes:list_sector')


class communityDeleteView(DeleteView):
    model = community
    template_name = "homes/delete_com.html"
    success_url = reverse_lazy("homes:list_community")

    def delete(self,request,*args, **kwargs):
        if request.is_ajax():
            community = self.get_object()
            community.delete()
            error = 'No hay error'
            mensaje = 'Comunidad Eliminada'
            response = JsonResponse({'mensaje': mensaje, 'error': error})
            response.status_code = 201
            return response
        else:
            return redirect('homes:list_community')

class sectorDeleteView(DeleteView):
    model = sector
    template_name = "homes/delete_sec.html"
    success_url = reverse_lazy("homes:list_sector")

    def delete(self,request,*args, **kwargs):
        if request.is_ajax():
            sector = self.get_object()
            sector.delete()
            error = 'No hay error'
            mensaje = 'Sector Eliminado'
            response = JsonResponse({'mensaje': mensaje, 'error': error})
            response.status_code = 201
            return response
        else:
            return redirect('homes:list_sector')


class homeDeleteView(DeleteView):
    model = home
    template_name = "homes/delete_home.html"
    success_url = reverse_lazy("homes:list_home")

    


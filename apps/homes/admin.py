from django.contrib import admin
from .models import *

class stateAdmin(admin.ModelAdmin):
    search_fields = ['name_state']
    list_display = ('name_state',)

class municipalityAdmin(admin.ModelAdmin):
    search_fields = ['name_municipality']
    list_display = ('name_municipality',)

admin.site.register(state,stateAdmin)
admin.site.register(municipality,municipalityAdmin)
admin.site.register(parish)
admin.site.register(community)
admin.site.register(sector)
admin.site.register(home)


# Register your models here.

from django.contrib.auth.decorators import login_required
from django.urls import path
from .views import *

urlpatterns = [
    path('create-home',login_required(homeCreateView.as_view()), name = 'create_home'),
    path("api-data", data, name="api_data"),
    path('create-community',login_required( communityCreateView.as_view()), name = 'create_community'),
    path('update-community/<int:pk>',login_required( communityUpdateView.as_view()), name = 'update_community'),
    path('update-sector/<int:pk>',login_required( sectorUpdateView.as_view()), name = 'update_sector'),
    path('create-sector',login_required( sectorCreateView.as_view()), name = 'create_sector'),
    path('list-sector',login_required( sectorListView.as_view()), name = 'list_sector'),
    path('list-community',login_required( communityListView.as_view()), name = 'list_community'),
    path('list-home',login_required(homeListView.as_view()), name = 'list_home'),
    path('edit-home/<int:pk>',login_required( homeUpdateView.as_view()), name = 'edit_home'),
    path('detail-home/<int:pk>',login_required( homeDetailView.as_view()), name = 'detail_home'),
    path('delete-home/<int:pk>',login_required( homeDeleteView.as_view()), name = 'delete_home'),
    path('delete-community/<int:pk>',login_required( communityDeleteView.as_view()), name = 'delete_community'),
    path('delete-sector/<int:pk>',login_required( sectorDeleteView.as_view()), name = 'delete_sector'),
]
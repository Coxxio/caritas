"""caritas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.decorators import login_required
from apps.homes.views import home_view
from apps.users.views import loginView,logoutUser

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('',login_required(home_view.as_view()), name = 'home'),
    path('accounts/login/',loginView.as_view(), name='login'),
    path('logout',logoutUser, name='logout'),
    path('homes/',include(('apps.homes.urls','homes'))),
    path('childs/',include(('apps.childs.urls','childs'))),
    path('church/',include(('apps.church.urls','church'))),
    path('supplies/',include(('apps.supplies.urls','supplies')))
]

function load_community(){
  var community = document.getElementById('id_community').value
  try{
    if (community){
      $('#id_id_community').val(community)
    }
    else{
      $('#id_id_community').val()
    }
  }
  catch(e) {}
}
function abrir_modal_creacion_sector(url) {
	$('#creacion_sector').load(url, function () {
		$(this).modal('show');
	});
}
function abrir_modal_delete_sector(url) {
	$('#delete_sector').load(url, function () {
		$(this).modal('show');
	});
}
function abrir_modal_update(url) {
	$('#update_sector').load(url, function () {
		$(this).modal('show');
	});
}
function cerrar_modal_creacion_sector(){
  $('#creacion_sector').modal('hide');
}
function cerrar_modal_update(){
  $('#update_sector').modal('hide');
}
function cerrar_modal_delete_sector(){
  $('#delete_sector').modal('hide');
}
function search_parish() { 
        var id =$('select[name="parish"]').val();
        var select_community = $('select[name="community"]');
        var options = '<option value="null">Seleccione una Comunidad</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_community_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_community.html(options);
      });
    }
  };
function search_sector() { 
  var id =$('select[name="community"]').val();
  var select_sec = $('tbody[id="table"]');
  var options = '<tr>'
  if (id != ''){
  $.ajax({
    url: window.location.pathname,
    type: 'POST',
    data: {
        'action': 'search_sector',
        'id': id
    },
    dataType: 'json',
}).done(function (data) {
    console.log(data.length)
    if (!data.hasOwnProperty('error')) {
      if (data.length != 0){
        $.each(data, function(key, value){
          options+='<tr>'
          options+='<td>'+ value.name +'</td>'
          options+=`<td class="text-center">
                          <div class="dropdown">
                          <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                  data-bs-toggle="dropdown" aria-expanded="false">
                                  Opciones
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                              <li><a class="dropdown-item" href="#" onclick="abrir_modal_update('update-sector/`+value.id+`')">Editar</a></li>
                              <li><a class="dropdown-item" href="#"  onclick="abrir_modal_delete_sector('delete-sector/`+value.id+`')">Eliminar</a></li>
                              </ul>
                              </div>
                              </td>`
                            })
          options += '</tr>'
        }else if (data.length == 0){
          Swal.fire({
            icon: 'error',
            title: 'No existen sectores registrados en esta comunidad',
            showConfirmButton: false,
            timer: 1500})
        }
        return false;
    }
    message_error(data.error);
}).fail(function (jqXHR, textStatus, errorThrown) {
    alert(textStatus + ': ' + errorThrown);
}).always(function (data) {
  select_sec.html(options);
});
}
  };

  function registrar_sector(){
    console.log('hola')
    $.ajax({
        data: $('#form_creacion').serialize(),
        url: $('#form_creacion').attr('action'),
        type: $('#form_creacion').attr('method'),
        success: function(response){
            console.log(response)
            cerrar_modal_creacion_sector()
            Swal.fire({
              icon: 'success',
              title: 'Sector registrado',
              showConfirmButton: false,
              timer: 1000})
            search_sector();
        },
        error: function(response){
            console.log(error)

        }
    })
}
function delete_community(pk){
  $.ajax({
    data:{
      csrfmiddlewaretoken:$("[name='csrfmiddlewaretoken']").val()
    },
    url: '/homes/delete-sector/'+pk,
    type: 'post',
    success: function(response){
        cerrar_modal_delete_sector()
        Swal.fire({
          icon: 'success',
          title: response['mensaje'],
          showConfirmButton: false,
          timer: 1000})
        search_parish();
    },
    error: function(response){
      Swal.fire({
        icon: 'error',
        title: response['mensaje'],
        showConfirmButton: false,
        timer: 1000})
      search_parish();

    }
})
}
function editar(){
  $.ajax({
      data: $('#form_edicion').serialize(),
      url: $('#form_edicion').attr('action'),
      type: $('#form_edicion').attr('method'),
      success: function (response) {
        console.log(response)
        cerrar_modal_update()
        Swal.fire({
          icon: 'success',
          title: response['mensaje'],
          showConfirmButton: false,
          timer: 1000})
        search_sector();
      },
      error: function (response) {
        Swal.fire({
          icon: 'error',
          title: response['mensaje'],
          showConfirmButton: false,
          timer: 1000})

      }
  });
}
function delete_sector(pk){
  $.ajax({
    data:{
      csrfmiddlewaretoken:$("[name='csrfmiddlewaretoken']").val()
    },
    url: '/homes/delete-sector/'+pk,
    type: 'post',
    success: function(response){
        cerrar_modal_delete_sector()
        Swal.fire({
          icon: 'error',
          title: response['mensaje'],
          showConfirmButton: false,
          timer: 1000}).then((result) => {
            search_sector();
          })
    },
    error: function(response){
      Swal.fire({
        icon: 'error',
        title: response['mensaje'],
        showConfirmButton: false,
        timer: 1000})
      search_parish();

    }
})
}
function abrir_modal_creacion_integrant(url,id) {
	$('#creacion_integrant').load(url, function () {
		$(this).modal('show');
    $('#id_church_group').val(id)
	});
}

function cerrar_modal_creacion_integrant(){
	$('#creacion_integrant').modal('hide');
}
function abrir_modal_creacion_group(url){
	$('#creacion_nursery').load(url, function () {
		$(this).modal('show');
	});
}
function abrir_modal_creacion_nursery(url,id) {
	$('#creacion_nursery').load(url, function () {
		$(this).modal('show');
    $('#id_church_group').val(id)
	});
}
function abrir_modal_eliminacion_nursery(url) {
	$('#eliminacion_nursery').load(url, function () {
		$(this).modal('show');
	});
}
function cerrar_modal_creacion_nursery(){
	$('#creacion_nursery').modal('hide');
}
function cerrar_modal_creacion_group(){
	$('#creacion_gruop').modal('hide');
}
$(function () { 
    $('select[name="state"]').on('change', function(){
        var id =$(this).val();
        var select_municipality = $('select[name="municipality"]');
        var options = '<option value="null">Seleccione un Municipio</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_municipality_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_municipality.html(options);
      });
    }
    });
  });
  $(function () { 
    $('select[name="municipality"]').on('change', function(){
        var id =$(this).val();
        var select_parish = $('select[name="parish"]');
        var options = '<option value="null">Seleccione una Parroquia</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_parish_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_parish.html(options);
      });
    }
    });
  });

  $(function () { 
    $('select[name="parish"]').on('change', function(){
        var id =$(this).val();
        var select_church = $('select[name="church"]');
        var options = '<option value="null"><strong>Seleccione una Iglesia</strong></option>'
        console.log(id)
        if (id != ''){
          $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_church',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
        alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_church.html(options);
      });
    }
    });
  });

function list_gruop() { 
        var id =$('select[id="id_church"]').val();
        var select_group = $('tbody[id="table"]');
        var options = '<tr>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_group',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          console.log(data)
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                  options+='<td>'+ value.name +'</td><td>'+value.integrants+'</td><td>'+value.nursery+'</td><td>'+value.president_name+' '+value.president_last_name+'</td>'
                  options+=`<td class="text-center">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        Opciones
                                    </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="#" onclick="abrir_modal_creacion_integrant('create-integrant',`+value.id+`);">Agregar Integrante</a></li>
                                    <li><a class="dropdown-item" href="#" onclick="abrir_modal_creacion_nursery('create-nursery',`+value.id+`);">Agregar Vivero</a></li>
                                    <li><a class="dropdown-item" href="#">Eliminar</a></li>
                                </ul>
                            </div>
                        </td><tr>`
                })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_group.html(options);
      });
    }
  };
  $(function () { 
    $('select[id="church_nursery"]').on('change', function(){
      var id =$(this).val();
      var select_church_group = $('select[name="church_group"]');
      var options = '<option value="null"><strong>Seleccione un Grupo Parroquial</strong></option>'
      $.ajax({
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'search_group',
            'id': id
        },
        dataType: 'json',
    }).done(function (data) {
        if (!data.hasOwnProperty('error')) {
            $.each(data, function(key, value){
              options+='<option value="'+ value.id + '">'+ value.name +'</option>'
            })
            return false;
        }
        message_error(data.error);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert(textStatus + ': ' + errorThrown);
    }).always(function (data) {
      select_church_group.html(options);
    });
  });
});
  $(function () { 
    $('select[name="church_group"]').on('change', function(){
        var id =$(this).val();
        var select_group = $('tbody[id="table"]');
        var options = '<tr>'
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_nursery',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          console.log(data)
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                  options+='<td>'+ value.name +'</td><td>'+value.start+'</td><td>'+value.end+'</td><td>'+value.integrants+'</td>'
                  if (value.status){
                    options+='<td>Abierto</td>'
                  }else if (!value.status){
                    options+='<td>Cerrado</td>'
                  }
                  options+=`<td class="text-center">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        Opciones
                                    </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="inform/`+value.id+`" >Generar Informe</a></li>
                                    <li><a class="dropdown-item" href="#" onclick="abrir_modal_eliminacion_nursery('delete-nursery/`+value.id+`');">Cerrar Viviero</a></li>
                                </ul>
                            </div>
                        </td><tr>`
                })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_group.html(options);
      });
    });
  });
  function registrar_integrant(){
    console.log('hola')
    $.ajax({
        data: $('#form_creacion_integrant').serialize(),
        url: $('#form_creacion_integrant').attr('action'),
        type: $('#form_creacion_integrant').attr('method'),
        success: function(response){
        cerrar_modal_creacion_integrant()
        list_gruop();

        },
        error: function(response){
            console.log(error)

        }
    })
}
function registrar_nursery(){
  console.log('hola')
  $.ajax({
      data: $('#form_creacion_nursery').serialize(),
      url: $('#form_creacion_nursery').attr('action'),
      type: $('#form_creacion_nursery').attr('method'),
      success: function(response){
          cerrar_modal_creacion_nursery()
          Swal.fire({
            icon: 'success',
            title: 'Vivero creado',
            showConfirmButton: false,
            timer: 1000}).then((result) => {
                list_gruop() 
            })

      },
      error: function(response){
        Swal.fire({
          icon: 'error',
          title: 'Error al crear',
          showConfirmButton: false,
          timer: 1000}).then((result) => {
          })

      }
  })
}
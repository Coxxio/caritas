$(function () { 
    $('select[name="state"]').on('change', function(){
        var id =$(this).val();
        var select_municipality = $('select[name="municipality"]');
        var options = '<option value="">Seleccione un Municipio</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_municipality_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_municipality.html(options);
      });
    }
    });
  });
  $(function () { 
    $('select[name="municipality"]').on('change', function(){
        var id =$(this).val();
        var select_parish = $('select[name="parish"]');
        var options = '<option value="">Seleccione una Parroquia</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_parish_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_parish.html(options);
      });
    }
    });
  });
  
  $(function () { 
    $('select[name="parish"]').on('change', function(){
        var id =$(this).val();
        var select_community = $('select[name="community"]');
        var options = '<option value="">Seleccione una comunidad</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_community_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_community.html(options);
      });
    }
    });
  });
  
  $(function () { 
    $('select[name="community"]').on('change', function(){
        var id =$(this).val();
        var select_sector = $('select[name="id_sector"]');
        var options = '<option value="">Seleccione un sector</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_sector_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_sector.html(options);
      });
    }
    });
  });
  
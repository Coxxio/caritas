function abrir_modal_creacion_person(url) {
  $('#creacion_person').load(url, function () {
    $('#creacion_chief').modal('hide');
    $('#list_person').modal('hide');
		$(this).modal('show');
	});
}
function abrir_modal_creacion_chief(url) {
  $('#creacion_chief').load(url, function () {
    $(this).modal('show');
	});
}
function buscar(url){
  $('#list_person').load(url, function () {
    $('#creacion_chief').modal('hide');
		$(this).modal('show');
	});
}
function seleccion_person(id){
	$('#list_person').modal('hide');
  $('#creacion_chief').modal('show');
  $('#chief_search').val(id)
}
function creacion_person(id){
	$('#list_person').modal('hide');
  $('#creacion_chief').modal('show');
  $('#chief_search').val(id)
}
function buscar_dinamica(){
  $('table tr').each(function() {
    // Obtengo el valor de dicho th
    var searching = document.getElementById('searching').value
    var value = $(this).text();
    searching = searching.toUpperCase()
    value = value.toUpperCase()
    // Acá deberías hacer lo que quieras con ese valor obtenido
    if (value != 'Seleccionar'){
      existe = value.includes(searching)
      someone = true
    }
    if (!existe){
      $(this).addClass('d-none')
    }else if(existe){
      $(this).removeClass('d-none')
    }
  });
  if (someone){
    $("#button_create").removeClass('invisible')
  }

}

function abrir_modal_creacion_familiar(url) {
	$('#creacion_familiar').load(url, function () {
		$(this).modal('show');
	});
}
function cerrar_modal_creacion_familiar(){
	$('#creacion_familiar').modal('hide');
}
function cerrar_modal_creacion_chief(){
	$('#creacion_chief').modal('hide');
}
function cerrar_modal_creacion_persona(){
	$('#creacion_person').modal('hide');
}

function home(){
  var home = document.getElementById('id_home').value
  try{
    if (home){
      $('#id_home_chief').val(home)
    }
    else{
      $('#id_home_chief').val()
    }
  }
  catch(e) {}
}

function score(){
  var c_arm = document.getElementById('id_c_arm').value
  var accepted = $('#id_accepted').is(":checked")
  var select_accepted = $('input[id="id_accepted"]');
  try{
    console.log(accepted)
    c_arm = (isNaN(parseInt(c_arm)))? 0 : parseInt(c_arm);
    if (c_arm < 110) {
      $('#id_z_score').val('-3')
    }else if (c_arm >= 110 && c_arm <= 124) {
      $('#id_z_score').val('-2')
    }else if (c_arm > 124 && c_arm <= 134) {
      $('#id_z_score').val('-1')
    }else if (c_arm > 134) {
      $('#id_z_score').val('0')
    }
    var z_score = document.getElementById('id_z_score').value
    if (z_score>0){
      select_accepted.prop("checked",false);
      console.log('Rechazado')
    }else {
      console.log('Rechazado')
    }

  }
  catch(e) {}
}
function chief(){
  var chief = document.getElementById('chief_search').value
  var familiar = document.getElementById('id_dni').value
  console.log(familiar)
  try{
    if (chief && familiar){
      $('#id_familiar_child').val(familiar)
      $('#chief_familiar').val(chief)
    }
    else{
      $('#id_familiar_child').val()
      $('#chief_familiar').val()
    }
  }
  catch(e) {}
}
$(function () { 
  $('select[id="id_familiar"]').on('change', function(){
      var id =$(this).val();
      var select_accepted = $('input[id="id_accepted"]');
      var options = ''
      $.ajax({
        url: window.location.pathname,
        type: 'POST',
        data: {
            'action': 'search_age',
            'id': id
        },
        dataType: 'json',
    }).done(function (data) {
        if (!data.hasOwnProperty('error')) {
          $.each(data, function(key, value){
            age = new Date(value.age)
            now = new Date()
            now.setMonth (now.getMonth() - 60);
            if (age>now){
              options = true
            }
            else if (age<now){
              options = false
            }
          })
            return false;
        }
        message_error(data.error);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert(textStatus + ': ' + errorThrown);
    }).always(function (data) {
      select_accepted.prop("checked",options);
    });
  });
});

function chief_child(){
  var chief = document.getElementById('chief_search').value
  var date = document.getElementById('id_birth_date').value
  var hoy = new Date();
  var hora = hoy.getHours() + hoy.getMinutes() + hoy.getSeconds();
  newdate = date.replaceAll('/', '');
  chief += newdate + hora
  try{
    if (chief){
      $('#id_dni').val(chief)
    }
    else{
      $('#id_dni').val()
    }
  }
  catch(e) {}
}
$(function () { 
    $('select[name="state"]').on('change', function(){
        var id =$(this).val();
        var select_municipality = $('select[name="municipality"]');
        var options = '<option value="null">Seleccione un Municipio</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_municipality_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_municipality.html(options);
      });
    }
    });
  });
  $(function () { 
    $('select[name="municipality"]').on('change', function(){
        var id =$(this).val();
        var select_parish = $('select[name="parish"]');
        var options = '<option value="null">Seleccione una Parroquia</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_parish_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_parish.html(options);
      });
    }
    });
  });
  
function search_parish() { 
        var id =$('select[name="parish"]').val();
        var select_community = $('select[name="community"]');
        var options = '<option value="null">Seleccione una Comunidad</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_community_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_community.html(options);
      });
    }
  };
  
function search_sector() { 
        var id =$('select[name="community"]').val();
        var select_sector = $('select[name="sector"]');
        var options = '<option value="null">Seleccione un Sector</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_sector',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_sector.html(options);
      });
    }
}

  $(function () { 
    $('select[name="sector"]').on('change', function(){
        var id =$(this).val();
        var select_home = $('select[name="home"]');
        var options = '<option value="null">Seleccione una Vivienda</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_home_1',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.direction +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_home.html(options);
      });
    }
    });
  });

function search_chief() { 
        var id =$('select[name="home"]').val();
        var select_home = $('select[id="id_chief"]');
        var options = '<option value="null">Seleccione un Representante</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_chief',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +' '+ value.last_name +' C.I.:' + value.dni +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_home.html(options);
      });
    }
  };


function search_child() { 
        var id =$('select[id="chief"]').val();
        var select_child = $('select[name="familiar"]');
        var options = '<option value="null">Seleccione un Niño</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_child',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +' '+ value.last_name +'  ('+ value.relation +')</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_child.html(options);
      });
    }
  }
    
function child_familiar(){
        var id =$('select[id="chief_search"]').val();
          var select_child = $('select[name="familiar"]');
          var select_nursery = $('select[name="nursery"]');
          var options = '<option value="null">Seleccione un Niño</option>'
          var options2 = '<option value="null">Seleccione un Vivero</option>'
          if (id != ''){
          $.ajax({
            url: window.location.pathname,
            type: 'POST',
            data: {
              'action': 'search_child',
              'id': id
            },
            dataType: 'json',
          }).done(function (data) {
            if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                if (value.id) {
                  options+='<option value="'+ value.id + '">'+ value.name +' '+ value.last_name +'  ('+ value.relation +')</option>'
                }
                  if (value.id_nursery) {
                    options2+='<option value="'+ value.id_nursery + '">'+ value.name_nursery +'</option>'
                  }
                })
                return false;
            }
            message_error(data.error);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert(textStatus + ': ' + errorThrown);
          }).always(function (data) {
          select_child.html(options);
          select_nursery.html(options2);
        });
      }
  };
  function registrar_person(){
    var id = document.getElementById('id_dni').value
    console.log('hola')
    $.ajax({
        data: $('#form_creacion_person').serialize(),
        url: $('#form_creacion_person').attr('action'),
        type: $('#form_creacion_person').attr('method'),
        success: function(response){
          cerrar_modal_creacion_persona();
          buscar('list-person');
            
            msg = document.getElementById("msg_alert")
            msg.innerHTML =  `<strong>`+response.mensaje+`</strong>`
            document.getElementById("alert").className = "d-flex justify-content-center container";

        },
        error: function(response){
            console.log(error)

        }
    })
}
function registrar_person_child(){
  console.log('hola2')
  $.ajax({
      data: $('#form_creacion_person').serialize(),
      url: $('#form_creacion_person').attr('action'),
      type: $('#form_creacion_person').attr('method'),
      success: function(response){
          cerrar_modal_creacion_persona()
          abrir_modal_creacion_familiar('/childs/create-familiar')

      },
      error: function(response){
        Swal.fire({
          icon: 'error',
          title: 'Ingrese Fecha Valida',
          showConfirmButton: false,
          timer: 1000}).then((result) => {
          })

      }
  })
}
function registrar_chief(){
  console.log('hola')
  $.ajax({
      data: $('#form_creacion_chief').serialize(),
      url: $('#form_creacion_chief').attr('action'),
      type: $('#form_creacion_chief').attr('method'),
      success: function(response){
          cerrar_modal_creacion_chief()
          Swal.fire({
            icon: 'success',
            title: 'Representante creado',
            showConfirmButton: false,
            timer: 1000}).then((result) => {
                search_chief()
            })

      },
      error: function(response){
          console.log(error)

      }
  })
}

function registrar_familiar(){
  $.ajax({
    data: $('#form_creacion_familiar').serialize(),
    url: $('#form_creacion_familiar').attr('action'),
    type: $('#form_creacion_familiar').attr('method'),
    success: function(response){
          cerrar_modal_creacion_familiar()
          child_familiar();
          select_child();

      },
      error: function(response){
          console.log(error)

      }
  })
}

function select_child(){
  var child = document.getElementById('id_familiar_child').value
  $('#id_familiar').val(child)
}
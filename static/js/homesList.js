$(function () { 
    $('select[name="state"]').on('change', function(){
        var id =$(this).val();
        var select_municipality = $('select[name="municipality"]');
        var options = '<option value="null">Seleccione un Municipio</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_municipality_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_municipality.html(options);
      });
    }
    });
  });
  $(function () { 
    $('select[name="municipality"]').on('change', function(){
        var id =$(this).val();
        var select_parish = $('select[name="parish"]');
        var options = '<option value="null">Seleccione una Parroquia</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_parish_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_parish.html(options);
      });
    }
    });
  });
  
  $(function () { 
    $('select[name="parish"]').on('change', function(){
        var id =$(this).val();
        var select_community = $('select[name="community"]');
        var options = '<option value="null">Seleccione una Comunidad</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_community_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_community.html(options);
      });
    }
    });
  });
  
  $(function () { 
    $('select[name="community"]').on('change', function(){
        var id =$(this).val();
        var select_sector = $('select[name="sector"]');
        var options = '<option value="null">Seleccione un Sector</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_sector',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_sector.html(options);
      });
    }
    });
  });
$(function () { 
    $('select[name="sector"]').on('change', function(){
        var id =$(this).val();
        var select_home = $('tbody[id="table"]');
        var options = '<tr>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_home',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          console.log(data)
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                  options+='<td>'+ value.direction +'</td><td>'+ value.type_floor +'</td><td>'+ value.type_wall +'</td><td>'+ value.type_roof+'</td>'
                  options+=`<td class="text-center">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        Opciones
                                    </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="edit-home/`+value.id+`">Editar</a></li>
                                    <li><a class="dropdown-item" href="#">Agregar Jefe Familiar</a></li>
                                    <li><a class="dropdown-item" href="delete-home/`+value.id+`">Eliminar</a></li>
                                </ul>
                            </div>
                        </td><tr>`
                })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_home.html(options);
      });
    }
    });
  });
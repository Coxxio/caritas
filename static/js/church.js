function abrir_modal_creacion_person(url) {
	$('#creacion_person').load(url, function () {
		$(this).modal('show');
	});
}
function abrir_modal_creacion_chief(url) {
	$('#creacion_chief').load(url, function () {
		$(this).modal('show');
	});
}
function abrir_modal_creacion_familiar(url) {
	$('#creacion_familiar').load(url, function () {
		$(this).modal('show');
	});
}
function abrir_modal_creacion_group(url,id){
  $('#creacion_group').load(url, function () {
		$(this).modal('show');
    $('#id_church').val(id)
	});
}
function cerrar_modal_creacion_group(){
	$('#creacion_group').modal('hide');
}
function search(){
  var busqueda = document.getElementById('buscar').value
  try{
      console.log(busqueda)
  }
  catch(e) {}
}
function cargar(){
  options=''
  $('#id_sector').html(options);
}
function clean_parish(){
  clean='<option value="null">---------</option>'
  $('select[id="id_parish"]').html(clean)
}
$(function () { 
    $('select[name="state"]').on('change', function(){
        var id =$(this).val();
        var select_municipality = $('select[name="municipality"]');
        var options = '<option value="null">Seleccione un Municipio</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_municipality_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_municipality.html(options);
      });
    }
    });
  });
  $(function () { 
    $('select[name="municipality"]').on('change', function(){
        var id =$(this).val();
        var select_parish = $('select[name="parish"]');
        var options = '<option value="null">Seleccione una Parroquia</option>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_parish_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_parish.html(options);
      });
    }
    });
  });
  $(function () { 
    $('select[id="parish_church"]').on('change', function(){
        var id =$(this).val();
        var select_parish = $('select[name="sector"]');
        var options = ''
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_parish_church',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_parish.html(options);
      });
    }
    });
  });

  $(function () { 
    $('select[id="id_parish"]').on('change', function(){
        var id =$(this).val();
        var select_church = $('tbody[id="table"]');
        var options = '<tr>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_church',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          console.log(data)
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                  options+='<td>'+ value.name +'</td><td>' + value.direction + '</td>'
                  options+=`<td class="text-center">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        Opciones
                                    </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="#" onclick="abrir_modal_creacion_group('create-church-group',`+value.id+`);">Agregar Grupo</a></li>
                                    <li><a class="dropdown-item" href="delete-church/`+value.id+`">Eliminar</a></li>
                                </ul>
                            </div>
                        </td><tr>`
                })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_church.html(options);
      });
    }
    });
  });
  
  function registrar_group(){
    console.log('hola2')
    $.ajax({
        data: $('#form_creacion_group').serialize(),
        url: $('#form_creacion_group').attr('action'),
        type: $('#form_creacion_group').attr('method'),
        success: function(response){
            cerrar_modal_creacion_group()
  
        },
        error: function(response){
            console.log(error)
  
        }
    })
  }
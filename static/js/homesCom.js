function load_parish(){
  var parish = document.getElementById('id_parish').value
  try{
    if (parish){
      $('#id_id_parish').val(parish)
    }
    else{
      $('#id_id_parish').val()
    }
  }
  catch(e) {}
}
function abrir_modal_creacion_community(url) {
	$('#creacion_community').load(url, function () {
		$(this).modal('show');
	});
}
function abrir_modal_delete_community(url) {
	$('#delete_community').load(url, function () {
		$(this).modal('show');
	});
}
function abrir_modal_update(url) {
	$('#update_community').load(url, function () {
		$(this).modal('show');
	});
}
function cerrar_modal_creacion_community(){
  $('#creacion_community').modal('hide');
}
function cerrar_modal_delete_community(){
  $('#delete_community').modal('hide');
}
function cerrar_modal_update(){
  $('#update_community').modal('hide');
}
function search_parish() { 
  console.log('hola')
        var id =$('select[name="parish"]').val();
        var select_com = $('tbody[id="table"]');
        var options = '<tr>'
        if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_community_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          console.log(data.length)
          if (!data.hasOwnProperty('error')) {
            if (data.length != 0){
              $.each(data, function(key, value){
                options+='<tr>'
                options+='<td>'+ value.name +'</td>'
                options+=`<td class="text-center">
                                <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        Opciones
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <li><a class="dropdown-item" href="#" onclick="abrir_modal_update('update-community/`+value.id+`')">Editar</a></li>
                                    <li><a class="dropdown-item" href="#" onclick="abrir_modal_delete_community('delete-community/`+value.id+`')">Eliminar</a></li>
                                    </ul>
                                    </div>
                                    </td>`
                                  })
                options += '</tr>'
              }else if (data.length == 0){
                Swal.fire({
                  icon: 'error',
                  title: 'No existen comunidades registradas en esta parroquia',
                  showConfirmButton: false,
                  timer: 1500})
              }
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_com.html(options);
      });
    }
  };

function registrar_community(){
    console.log('hola')
    $.ajax({
        data: $('#form_creacion').serialize(),
        url: $('#form_creacion').attr('action'),
        type: $('#form_creacion').attr('method'),
        success: function(response){
            console.log(response)
            cerrar_modal_creacion_community()
            Swal.fire({
              icon: 'success',
              title: response['mensaje'],
              showConfirmButton: false,
              timer: 1000})
            search_parish();
        },
        error: function(response){
          Swal.fire({
            icon: 'error',
            title: response['mensaje'],
            showConfirmButton: false,
            timer: 1000})

        }
    })
}
function editar(){
  $.ajax({
      data: $('#form_edicion').serialize(),
      url: $('#form_edicion').attr('action'),
      type: $('#form_edicion').attr('method'),
      success: function (response) {
        console.log(response)
        cerrar_modal_update()
        Swal.fire({
          icon: 'success',
          title: response['mensaje'],
          showConfirmButton: false,
          timer: 1000})
        search_parish();
      },
      error: function (response) {
        Swal.fire({
          icon: 'error',
          title: response['mensaje'],
          showConfirmButton: false,
          timer: 1000})

      }
  });
}
function delete_community(pk){
  $.ajax({
    data:{
      csrfmiddlewaretoken:$("[name='csrfmiddlewaretoken']").val()
    },
    url: '/homes/delete-community/'+pk,
    type: 'post',
    success: function(response){
        cerrar_modal_delete_community()
        Swal.fire({
          icon: 'error',
          title: response['mensaje'],
          showConfirmButton: false,
          timer: 1000})
        search_parish();
    },
    error: function(response){
      Swal.fire({
        icon: 'error',
        title: response['mensaje'],
        showConfirmButton: false,
        timer: 1000})
      search_parish();

    }
})
}
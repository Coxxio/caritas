function abrir_modal_delivery(url) {
	$('#delivery_supplie').load(url, function () {
		$(this).modal('show');
	});
}
function cerrar_modal_delivery(){
    $('#delivery_supplie').modal('hide');
}
  
  function search_church() { 
        var id =$('select[name="parish"]').val();
        var select_church = $('select[name="church"]');
        var options = '<option value="null">Seleccione una Iglesia</option>'
        if (id != ''){
          if (id != ''){
        $.ajax({
          url: window.location.pathname,
          type: 'POST',
          data: {
              'action': 'search_church_id',
              'id': id
          },
          dataType: 'json',
      }).done(function (data) {
          if (!data.hasOwnProperty('error')) {
              $.each(data, function(key, value){
                options+='<option value="'+ value.id + '">'+ value.name +'</option>'
              })
              return false;
          }
          message_error(data.error);
      }).fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ': ' + errorThrown);
      }).always(function (data) {
        select_church.html(options);
      });
    }
  }
  };
  function search_group() { 
    var id =$('select[id="id_church"]').val();
    var select_church_group = $('select[name="church_group"]');
    var options = '<option value="null"><strong>Seleccione un Grupo Parroquial</strong></option>'
    if (id != ''){
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'search_group',
                'id': id,
            },
            dataType: 'json',
        }).done(function (data) {
            if (!data.hasOwnProperty('error')) {
                $.each(data, function(key, value){
                  options+='<option value="'+ value.id + '">'+ value.name +'</option>'
                })
                return false;
            }
            message_error(data.error);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert(textStatus + ': ' + errorThrown);
        }).always(function (data) {
          select_church_group.html(options);
        });
      }
    };
    function search_nursery() { 
      var id =$('select[id="id_church"]').val();
      var select_nursery = $('select[name="nursery"]');
      var options = '<option value="null"><strong>Seleccione un Vivero</strong></option>'
      if (id != ''){
          $.ajax({
              url: window.location.pathname,
              type: 'POST',
              data: {
                  'action': 'search_nursery',
                  'id': id,
              },
              dataType: 'json',
          }).done(function (data) {
              if (!data.hasOwnProperty('error')) {
                  $.each(data, function(key, value){
                    options+='<option value="'+ value.id + '">'+ value.name +'</option>'
                  })
                  return false;
              }
              message_error(data.error);
          }).fail(function (jqXHR, textStatus, errorThrown) {
              alert(textStatus + ': ' + errorThrown);
          }).always(function (data) {
            select_nursery.html(options);
          });
        }
      };
    function search_childs() { 
      var id =$('select[id="id_nursery"]').val();
      var select_church_group = $('tbody[id="table"]');
      var options = ''
      if (id != ''){
          $.ajax({
              url: window.location.pathname,
              type: 'POST',
              data: {
                  'action': 'search_childs',
                  'id': id,
              },
              dataType: 'json',
          }).done(function (data) {
              if (!data.hasOwnProperty('error')) {
                    if (data.length != 0){
                      $.each(data, function(key, value){
                        options+='<tr>'
                        options+=`<td>`+value.representante_name +` `+ value.representante_last_name+` C.I.:(`+value.representante_dni+`)</td>
                          <td>`+ value.name +` `+value.last_name+` (`+value.relation+`)</td>
                          <td>`+value.zscore+`</td>
                          <td>`+value.delivery+`</td>
                          `
                        options+=`<td class="text-center">
                                        <button class="btn btn-primary" type="button" onclick="delivery(`+value.id_data+`)">
                                                Entregar
                                            </button>
                                            </td>`
                      })
                        options += '</tr>'
                      }else if (data.length == 0){
                        Swal.fire({
                          icon: 'error',
                          title: 'No existen niños aptos para entrar al programa',
                          showConfirmButton: false,
                          timer: 1500})
                      }
                  return false;
              }
              message_error(data.error);
          }).fail(function (jqXHR, textStatus, errorThrown) {
              alert(textStatus + ': ' + errorThrown);
          }).always(function (data) {
            select_church_group.html(options);
          });
        }
      };

function delivery(id){
  console.log('hola')
  $.ajax({
    data: {
      'action': 'delivery',
      'id': id,
  },
    url: window.location.pathname,
    type: 'POST',
      success: function(response){
        console.log(response)
          Swal.fire({
            icon: 'success',
            title: 'Lote Delegado',
            showConfirmButton: false,
            timer: 1000}).then((result) => {
              search_childs()
            })
      },
      error: function(response){
        Swal.fire({
          icon: 'error',
          title: 'El valor es mayor a la cantidad disponible',
          showConfirmButton: false,
          timer: 1000})
      }
  })
}
function abrir_modal_creacion_supplie(url) {
	$('#creacion_supplie').load(url, function () {
		$(this).modal('show');
	});
}
function abrir_modal_creacion_lote(url) {
	$('#creacion_lote').load(url, function () {
		$(this).modal('show');
	});
}
function abrir_modal_delegate_supplie(url,id,max) {
	$('#delegate_supplie').load(url,id, function () {
        $(this).modal('show');
        $('#id_lote').val(id);
        $('#maximo').html(max)
        $('#id_stock').attr({"max":max})
	});
}
function abrir_modal_list_group(url) {
	$('#list_church_group').load(url, function () {
		$(this).modal('show');
	});
}
function cerrar_modal_creacion_supplie(){
    $('#creacion_supplie').modal('hide');
}
function cerrar_modal_creacion_lote(){
    $('#creacion_lote').modal('hide');
}
function cerrar_modal_delegate_supplie(){
    $('#delegate_supplie').modal('hide');
}
function activar_boton(){
    if($('#boton_creacion').prop('disabled')){
        $('#boton_creacion').prop('disabled',false);
    }else{
        $('#boton_creacion').prop('disabled',true);
    }
}
function load_lote(){
    var options = '<option value="null"><strong>--------</strong></option>'
    $('select[name="group"]').html(options);
}
function registrar_supplie(){
    console.log('hola')
    $.ajax({
        data: $('#form_creacion').serialize(),
        url: $('#form_creacion').attr('action'),
        type: $('#form_creacion').attr('method'),
        success: function(response){
            console.log(response)
            cerrar_modal_creacion_supplie()
            Swal.fire({
                icon: 'success',
                title: 'Insumo creados',
                showConfirmButton: false,
                timer: 1000}).then((result) => {
                    location.reload();
                })
        },
        error: function(response){
            console.log(error)

        }
    })
}
function registrar_lote(){
    console.log('hola')
    $.ajax({
        data: $('#form_creacion').serialize(),
        url: $('#form_creacion').attr('action'),
        type: $('#form_creacion').attr('method'),
        success: function(response){
            console.log(response)
            cerrar_modal_creacion_lote()
            Swal.fire({
                icon: 'success',
                title: 'Lote registrado',
                showConfirmButton: false,
                timer: 1000}).then((result) => {
                    location.reload();
                })
        },
        error: function(response){
            console.log(error)

        }
    })
}
function delegate_supplie(){
    console.log('hola')
    $.ajax({
        data: $('#form_delegate').serialize(),
        url: $('#form_delegate').attr('action'),
        type: $('#form_delegate').attr('method'),
        success: function(response){
            console.log(response)
            cerrar_modal_delegate_supplie()
            Swal.fire({
                icon: 'success',
                title: 'Lote Delegado',
                showConfirmButton: false,
                timer: 1000}).then((result) => {
                    location.reload();
                })
        },
        error: function(response){
            Swal.fire({
                icon: 'error',
                title: 'El valor es mayor a la cantidad disponible',
                showConfirmButton: false,
                timer: 1000})
        }
    })
}
function multi(){
    var stock = document.getElementById('id_stock').value
    if (stock == 0){
        Swal.fire({
            icon:'error',
            title:'La cantidad debe ser mayor a 0',
            showConfirmButton: false,
            timer:1000
        }).then((result)=> {
            $('#id_stock').val('')
        })
    }
    try{
        console.log(stock)
        stock = (isNaN(parseInt(stock)))? 0 : parseInt(stock);
        $('#id_total').val(stock * 150)
    }
    catch(e) {}
}
function search_church() { 
        var id =$('select[name="parish"]').val();
        var select_church = $('select[name="church"]');
        var options = '<option value="null"><strong>Seleccione una Iglesia</strong></option>'
        if (id != ''){
            $.ajax({
                url: window.location.pathname,
                type: 'POST',
                data: {
                    'action': 'search_church',
                    'id': id,
                    csrfmiddlewaretoken:$("[name='csrfmiddlewaretoken']").val()
                },
            dataType: 'json',
        }).done(function (data) {
            if (!data.hasOwnProperty('error')) {
                $.each(data, function(key, value){
                  options+='<option value="'+ value.id + '">'+ value.name +'</option>'
                })
                return false;
            }
            message_error(data.error);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                showConfirmButton: false,
                timer: 1000})
        }).always(function (data) {
          select_church.html(options);
        });
      }
    };
function search_group() { 
    var id =$('select[id="id_church"]').val();
    var select_church_group = $('select[name="group"]');
    var options = '<option value="null"><strong>Seleccione un Grupo Parroquial</strong></option>'
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'search_group',
                'id': id,
                csrfmiddlewaretoken:$("[name='csrfmiddlewaretoken']").val()

            },
            dataType: 'json',
        }).done(function (data) {
            if (!data.hasOwnProperty('error')) {
                $.each(data, function(key, value){
                  options+='<option value="'+ value.id + '">'+ value.name +'</option>'
                })
                return false;
            }
            message_error(data.error);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert(textStatus + ': ' + errorThrown);
        }).always(function (data) {
          select_church_group.html(options);
        });
    };
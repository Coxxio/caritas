var speedCanvas = document.getElementById("speedChart");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var dataFirst = {
    label: "Manejo preventivo",
    data: [0, 0, 0, 0, 0, 0, 0,0,0,3,4,2],
    borderColor: 'yellow'
  };

var dataSecond = {
    label: "Moderado",
    data: [0, 0, 0, 0, 0, 0, 0,0,0,3,4,2],
  borderColor: 'orange'
  };
var dataThird = {
    label: "Severo",
    data: [0, 0, 0, 0,0, 0,0,0,0,3,4,2],
  borderColor: 'red'
  };

var speedData = {
  labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
  datasets: [dataFirst, dataSecond,dataThird]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 150,
      fontColor: 'black'
    }
  }
};

var lineChart = new Chart(speedCanvas, {
  type: 'line',
  data: speedData,
  options: chartOptions
});
